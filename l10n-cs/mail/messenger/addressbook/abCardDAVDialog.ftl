# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

carddav-window =
    .title = Nová složka kontaktů CardDAV
carddav-dialog =
    .buttonlabelaccept = Pokračovat
    .buttonaccesskeyaccept = P
carddav-experimental-warning = Podpora složek kontaktů CardDAV je experimentální a může trvale poškodit vaše data. Jejich používání je na vlastní nebezpečí.
carddav-provider-label =
    .value = Poskytovatel CardDAV:
    .accesskey = P
carddav-provider-option-other = Další poskytovatel…
carddav-url-label =
    .value = URL adresa CardDAV:
    .accesskey = V
carddav-username-label =
    .value = Uživatelské jméno:
    .accesskey = U
carddav-username-input =
    .placeholder = vase-adresa@example.com
carddav-password-label =
    .value = Heslo:
    .accesskey = H
carddav-password-input =
    .placeholder = Heslo
carddav-remember-password =
    .label = Pamatovat si heslo
    .accesskey = m
carddav-loading = Vyhledávání nastavení…
carddav-connection-error = Chyba spojení.
carddav-none-found = U zadaného účtu nebyly nalezeny žádné složky kontaktů, které by bylo možné přidat.
carddav-already-added = Všechny složky kontaktů zadaného účtu už jsou přidány.
carddav-available-books = Dostupné složky kontaktů:
