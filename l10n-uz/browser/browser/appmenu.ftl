# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Moslash…

## Zoom Controls

appmenuitem-new-window =
    .label = Yangi oyna
appmenuitem-new-private-window =
    .label = Yangi maxfiy oyna

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Kattalashtirish
appmenuitem-zoom-reduce =
    .label = Kichiklashtirish
appmenuitem-fullscreen =
    .label = Butun ekran

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sinxronlash
appmenuitem-save-page =
    .label = Sahifani saqlash…

## What's New panel in App menu.

whatsnew-panel-header = Yangi xususiyatlar

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = { -brand-shorter-name } haqida
    .accesskey = h
appmenu-help-troubleshooting-info =
    .label = Nosozlik ma’lumoti
    .accesskey = N
appmenu-help-taskmanager =
    .label = Vazifa menejeri
appmenu-help-report-site-issue =
    .label = Saytdagi muammo haqida xabar berish
appmenu-help-feedback-page =
    .label = Mulohaza bildirish
    .accesskey = b

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Qo‘sh. dasturlarni o‘chirib, qayta ishga tushirish
    .accesskey = q
appmenu-help-safe-mode-with-addons =
    .label = Qo‘sh. dasturlarni yoqib, qayta ishga tushirish
    .accesskey = q

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Qalbaki sayt haqida xabar berish…
    .accesskey = Q
appmenu-help-not-deceptive =
    .label = Bu sayt qalbaki emas…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = Vazifa menejeri
