# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = { -brand-shorter-name } کی تذکاری ڈونلوڈ کر رہے ہیں
appmenuitem-protection-dashboard-title = حفاظتی ڈیش بورڈ
appmenuitem-customize-mode =
    .label = تخصیص کریں…

## Zoom Controls

appmenuitem-new-window =
    .label = نیا ونڈوں
appmenuitem-new-private-window =
    .label = نیا نجی ونڈوں

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = اندر زوم کریں
appmenuitem-zoom-reduce =
    .label = باہر زوم کریں
appmenuitem-fullscreen =
    .label = پوری اسکرین

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = ابھی Sync کریں
appmenuitem-save-page =
    .label = صفحہ محفوظ کریں بطور…

## What's New panel in App menu.

whatsnew-panel-header = نیا کیا ہے
# Checkbox displayed at the bottom of the What's New panel, allowing users to
# enable/disable What's New notifications.
whatsnew-panel-footer-checkbox =
    .label = نئی خصوصیات کے بارے میں مطلع کریں
    .accesskey = f

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".

profiler-popup-learn-more = مزیدجانیے
profiler-popup-settings =
    .value = سیٹنگز
# This link takes the user to about:profiling, and is only visible with the Custom preset.
profiler-popup-edit-settings = سیٹنگز میں تدوین کریں…
profiler-popup-recording-screen = ریکارڈنگ ہو رہا…
# The profiler presets list is generated elsewhere, but the custom preset is defined
# here only.
profiler-popup-presets-custom =
    .label = مخصوص
profiler-popup-start-recording-button =
    .label = ریکارڈنگ شروع کریں
profiler-popup-discard-button =
    .label = رد کريں
profiler-popup-capture-button =
    .label = ریکارڈ کریں

## History panel

appmenu-manage-history =
    .label = تاریخ کا نظم کریں
appmenu-reopen-all-tabs = تمام ٹیب کو دوبارہ کھولیں
appmenu-reopen-all-windows = تمام ونڈوز کو دوبارہ کھولیں

## Help panel

appmenu-help-header =
    .title = { -brand-shorter-name } کی مدد
appmenu-about =
    .label = { -brand-shorter-name } کے بارے میں
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = ازالہ کاری معلومات
    .accesskey = T
appmenu-help-taskmanager =
    .label = ٹاسک مینیجر
appmenu-help-report-site-issue =
    .label = سائٹ مسلہ… رپورٹ کریں
appmenu-help-feedback-page =
    .label = اپنی رائے بھیجیں...
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = ایڈ اون نا اہل کر کے دوباره شروع کریں
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = ایڈ اون اہل کر کے دوباره شروع کریں
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = پر فریب سایٹ کی رپورٹ کریں…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = یہ فریبی سائٹ نہیں ہے…
    .accesskey = ف

## More Tools

appmenu-customizetoolbar =
    .label = ٹول بار کی تخصیص کریں…
appmenu-taskmanager =
    .label = ٹاسک مینیجر
appmenu-developer-tools-subheader = براؤزر ٹولز
