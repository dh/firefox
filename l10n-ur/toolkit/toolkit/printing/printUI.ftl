# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

printui-title = چھاپیں
# Dialog title to prompt the user for a filename to save print to PDF.
printui-save-to-pdf-title = محفوظ کریں بطور
printui-page-range-all = تمام
printui-page-range-custom = مخصوص
printui-page-range-label = صفحات
printui-page-range-picker =
    .aria-label = صفحہ کی حد منتخب کریں
# This label is displayed before the first input field indicating
# the start of the range to print.
printui-range-start = منجانب:
# This label is displayed between the input fields indicating
# the start and end page of the range to print.
printui-range-end = بنام
# Section title for the number of copies to print
printui-copies-label = نقول
printui-orientation = سمت بندی
printui-landscape = افقی انداز
printui-portrait = عمودی انداز
# Section title for the printer or destination device to target
printui-destination-label = منزل:
printui-destination-pdf-label = بطور PDF محفوظ کریں
printui-more-settings = مزید ترتیبات
printui-less-settings = کم ترتیبات
printui-paper-size-label = کاغذ کا ماپ:
# Section title (noun) for the print scaling options
printui-scale = اسکیل
# Label for input control where user can set the scale percentage
printui-scale-pcent = اسکیل
# Section title (noun) for the two-sided print options
printui-two-sided-printing = دو طرفہ چھپائی
printui-duplex-checkbox = دونوں طرف چھاپیں
# Section title for miscellaneous print options
printui-options = اختیارات
printui-backgrounds-checkbox = پس منظر چھاپیں
printui-color-mode-color = رنگ
printui-color-mode-bw = سیاہ و سفید
printui-margins = حاشيے
printui-margins-default = طے شدہ
printui-margins-min = کم سے کم
printui-margins-none = کوئی نہیں
printui-margins-custom-left = بایاں
printui-margins-custom-left-inches = بائیں (انچ)
printui-margins-custom-right = دایاں
printui-margins-custom-right-inches = بائیں (انچ)
printui-primary-button = چھاپیں
printui-primary-button-save = محفوظ کریں
printui-cancel-button = منسوخ کریں
printui-close-button = بند کریں
# Reported by screen readers and other accessibility tools to indicate that
# the print preview has focus.
printui-preview-label =
    .aria-label = چھپائی کا  پیش نظارہ

## Paper sizes that may be supported by the Save to PDF destination:

printui-paper-a5 = A5
printui-paper-a4 = A4
printui-paper-a3 = A3
printui-paper-a2 = A2
printui-paper-a1 = A1
printui-paper-a0 = A0
printui-paper-b5 = B5
printui-paper-b4 = B4
printui-paper-jis-b5 = JIS-B5
printui-paper-jis-b4 = JIS-B4
printui-paper-tabloid = ٹیبلوئڈ

## Error messages shown when a user has an invalid input

