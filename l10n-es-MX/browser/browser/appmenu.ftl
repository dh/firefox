# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = Descargar la actualización de { -brand-shorter-name }
appmenuitem-protection-dashboard-title = Panel de protecciones
appmenuitem-customize-mode =
    .label = Personalizar…

## Zoom Controls

appmenuitem-new-window =
    .label = Nueva ventana
appmenuitem-new-private-window =
    .label = Nueva ventana privada

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Acercarse
appmenuitem-zoom-reduce =
    .label = Alejarse
appmenuitem-fullscreen =
    .label = Pantalla completa

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sincronizar ahora
appmenuitem-save-page =
    .label = Guardar como…

## What's New panel in App menu.

whatsnew-panel-header = Novedades
# Checkbox displayed at the bottom of the What's New panel, allowing users to
# enable/disable What's New notifications.
whatsnew-panel-footer-checkbox =
    .label = Notificar sobre nuevas funciones
    .accesskey = f

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Acerca de { -brand-shorter-name }
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = Información para solucionar problemas
    .accesskey = I
appmenu-help-taskmanager =
    .label = Administrador de tareas
appmenu-help-report-site-issue =
    .label = Reportar problema con el sitio…
appmenu-help-feedback-page =
    .label = Enviar comentarios...
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Reiniciar con complementos deshabilitados
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = Reiniciar con complementos habilitados
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Reportar sitio fraudulento…
    .accesskey = f
appmenu-help-not-deceptive =
    .label = Este no es un sitio engañoso…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = Administrador de tareas
