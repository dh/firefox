# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The question portion of the following message should have the <strong> and </strong> tags surrounding it.
default-browser-notification-message = <strong>{ -brand-short-name } ਨੂੰ ਆਪਣਾ ਡਿਫਾਲਟ ਬਰਾਊਜ਼ਰ ਬਣਾਉਣਾ ਹੈ?</strong> ਜਦੋਂ ਵੀ ਤੁਸੀਂ ਵੈਬ ਨੂੰ ਵਰਤੋਂ ਤਾਂ ਤੇਜ਼, ਸੁਰੱਖਿਅਤ ਅਤੇ ਪ੍ਰਾਈਵੇਟ ਢੰਗ ਨਾਲ ਬਰਾਊਜ਼ ਕਰੋ।
default-browser-notification-button =
    .label = ਡਿਫਾਲਟ ਸੈੱਟ ਕਰੋ
    .accesskey = S
