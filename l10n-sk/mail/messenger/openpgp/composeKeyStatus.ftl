# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-compose-key-status-intro-need-keys = Ak chcete odoslať obojstranne šifrovanú správu, musíte získať a prijať verejný kľúč pre každého príjemcu.
openpgp-compose-key-status-keys-heading = Dostupnosť kľúčov OpenPGP:
openpgp-compose-key-status-title =
    .title = Bezpečnosť správy OpenPGP
openpgp-compose-key-status-recipient =
    .label = Adresát
openpgp-compose-key-status-status =
    .label = Stav
openpgp-compose-key-status-open-details = Spravovať kľúče pre vybratého príjemcu…
openpgp-recip-good = ok
openpgp-recip-missing = žiadny kľúč nie je dostupný
openpgp-recip-none-accepted = žiadne kľúč nebol prijatý
