# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profiledowngrade-window =
    .title = Spustili ste staršiu verziu aplikácie { -brand-product-name }
    .style = width: 490px;
profiledowngrade-window-create =
    .label = Vytvoriť nový profil
profiledowngrade-sync = Používaním staršej verzie prehliadača { -brand-product-name } môžete poškodiť záložky a históriu prehliadania, ktoré sú uložené vo vašom existujúcom profile. Aby ste svoje údaje ochránili, vytvorte si nový profil pre túto verziu prehliadača { -brand-short-name }. Následne sa môžete prihlásiť pomocou účtu { -fxaccount-brand-name } a zosynchronizovať svoje záložky a históriu prehliadania s ostatnými profilmi.
profiledowngrade-nosync = Používaním staršej verzie { -brand-product-name }u môžete poškodiť záložky a históriu prehliadania, ktoré sú uložené vo vašom existujúcom profile. Aby ste svoje údaje ochránili, vytvorte si nový profil pre túto verziu aplikácie { -brand-short-name }.
profiledowngrade-quit =
    .label =
        { PLATFORM() ->
            [windows] Ukončiť
           *[other] Ukončiť
        }
