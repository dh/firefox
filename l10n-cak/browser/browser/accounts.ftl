# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Used as the FxA toolbar menu item value when user has not
# finished setting up an account.
account-finish-account-setup = Tik'is Runuk'ulem K'ak'a' Rub'i' Taqoya'l
# Used as the FxA toolbar menu item title when the user
# needs to reconnect their account.
account-reconnect-to-fxa = Chupun Rub'i' Taqoya'l
# Used as the FxA toolbar menu item title when the user
# needs to reconnect their account.
account-disconnected = Xachüp
