# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-toolbar-sync-syncing =
    .label = Synchrōnizacyjo…
fxa-toolbar-sync-syncing-tabs =
    .label = Synchrōnizowanie kart…
fxa-toolbar-sync-syncing2 = Synchrōnizacyjo…
sync-disconnect-dialog-title = Rozłōnczyć { -sync-brand-short-name(case: "acc") }?
sync-disconnect-dialog-title2 = Rozłōnczyć?
sync-disconnect-dialog-body = { -brand-product-name } skōńczy synchrōnizować twoje kōnto, ale niy skasuje żodnych danych przeglōndanio na tyj maszinie.
fxa-disconnect-dialog-title = Rozłōnczyć aplikacyjo { -brand-product-name }?
fxa-disconnect-dialog-body = { -brand-product-name } ôdłōnczy sie ôd twojigo kōnta, ale niy skasuje żodnych danych przeglōndanio na tyj maszinie.
sync-disconnect-dialog-button = Rozłōncz…
fxa-signout-dialog-heading = Wylogować sie ze { -fxaccount-brand-name(case: "gen", capitalization: "lower") }?
fxa-signout-dialog2-title = Wylogować sie ze { -fxaccount-brand-name }?
fxa-signout-dialog-body = Synchronizowane dane ôstanōm na twojim kōncie.
fxa-signout-checkbox =
    .label = Skasuj dane z tyj masziny (dane logowanio, hasła, historyjo, zokłodki i inksze).
fxa-signout-dialog =
    .title = Wyloguj sie ze { -fxaccount-brand-name(case: "gen", capitalization: "lower") }
    .style = min-width: 375px;
    .buttonlabelaccept = Wyloguj sie
fxa-signout-dialog2-button = Wyloguj
fxa-signout-dialog2-checkbox = Skasuj dane z tyj masziny (hasła, historyjo, zokłodki i inksze)
fxa-menu-sync-settings =
    .label = Synchrōnizacyjo sztalōnkōw
fxa-menu-turn-on-sync =
    .value = Załōncz synchronizacyjo
fxa-menu-turn-on-sync-default = Załōncz synchronizacyjo
fxa-menu-connect-another-device =
    .label = Połōncz inkszo maszina…
fxa-menu-sign-out =
    .label = Wyloguj sie…
