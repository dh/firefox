# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The question portion of the following message should have the <strong> and </strong> tags surrounding it.
default-browser-notification-message = <strong>Nastawić aplikacyjo { -brand-short-name } za bazowo przeglōndarka?</strong> Poradzisz ś niōm gibko, bezpiecznie i prywatnie przeglōndać internet.
default-browser-notification-button =
    .label = Nastow za bazowo
    .accesskey = N
