# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = Ôdświyż aplikacyjo { -brand-short-name }
refresh-profile-dialog-button =
    .label = Ôdświyż aplikacyjo { -brand-short-name }
refresh-profile-description = Zacznij ôd nowa, coby rozwiōnzać problymy i wrōcić sprowność.
refresh-profile-description-details = Bez to:
refresh-profile-remove = Twoje rozszyrzynia i włosne sztalōnki bydōm skasowane
refresh-profile-restore = Sztalōnki przeglōndarki wrōcōm do bazowych
refresh-profile = Napasuj aplikacyjo { -brand-short-name }
refresh-profile-button = Ôdświyż aplikacyjo { -brand-short-name }…
