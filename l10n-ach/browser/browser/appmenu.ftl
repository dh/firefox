# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = Gamo ngec manyen pi { -brand-shorter-name }
appmenuitem-customize-mode =
    .label = Yiki…

## Zoom Controls

appmenuitem-new-window =
    .label = Dirica manyen
appmenuitem-new-private-window =
    .label = Dirica manyen me mung

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Kwot madit
appmenuitem-zoom-reduce =
    .label = Jwik matidi
appmenuitem-fullscreen =
    .label = Wang komputa ma opong

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Rib Kombedi
appmenuitem-save-page =
    .label = Gwok pot buk calo…

## What's New panel in App menu.

whatsnew-panel-header = Ngo Manyen
# Checkbox displayed at the bottom of the What's New panel, allowing users to
# enable/disable What's New notifications.
whatsnew-panel-footer-checkbox =
    .label = Mi ngec pi jami manyen
    .accesskey = m

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Ikom { -brand-shorter-name }
    .accesskey = I
appmenu-help-troubleshooting-info =
    .label = Ngec me yubu bal
    .accesskey = N
appmenu-help-report-site-issue =
    .label = Mi ripot ikom peko me kakube…
appmenu-help-feedback-page =
    .label = Cwal adwogi ne…
    .accesskey = C

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Cak odoco ki med-ikome gi ma kijuko woko…
    .accesskey = C
appmenu-help-safe-mode-with-addons =
    .label = Cak odoco kun nongo kicako med-ikome
    .accesskey = C

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Mi ripot i kom kakube me bwola…
    .accesskey = b
appmenu-help-not-deceptive =
    .label = Man pe kakube me bwola…
    .accesskey = b

## More Tools

