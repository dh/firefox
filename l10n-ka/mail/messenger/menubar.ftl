# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = ტექსტის კოდირება
    .accesskey = კ

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = ხარვეზის აღმოფხვრის რეჟიმი…
    .accesskey = ხ
menu-help-exit-troubleshoot-mode =
    .label = ხარვეზის აღმოფხვრის რეჟიმის გამორთვა
    .accesskey = გ
menu-help-more-troubleshooting-info =
    .label = ხარვეზის აღმოფხვრის ვრცელი მონაცემები
    .accesskey = ნ

## Mail Toolbar

toolbar-junk-button =
    .label = ჯართი
    .tooltiptext = მონიშნეთ წერილი უსარგებლოდ
toolbar-not-junk-button =
    .label = არაა ჯართი
    .tooltiptext = მონიშნეთ წერილი გამოსადეგად
toolbar-delete-button =
    .label = წაშლა
    .tooltiptext = შერჩეული წერილების ან საქაღალდის წაშლა
toolbar-undelete-button =
    .label = აღდგენა
    .tooltiptext = შერჩეული წერილების დაბრუნება
