# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Hanse war boŋ se…

## Zoom Controls

appmenuitem-new-window =
    .label = Zanfun taaga
appmenuitem-new-private-window =
    .label = Sutura zanfun taaga

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = Dijikul

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sync sohõ
appmenuitem-save-page =
    .label = Moɲoo gaabu sanda…

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = { -brand-shorter-name } ga
    .accesskey = a
appmenu-help-troubleshooting-info =
    .label = Karkahattayan alhabar
    .accesskey = K
appmenu-help-feedback-page =
    .label = Furari sanba…
    .accesskey = s

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Tunandi taaga kaŋ tontoney kayandi…
    .accesskey = u
appmenu-help-safe-mode-with-addons =
    .label = Tunandi taaga kaŋ tontoney ga kayandi
    .accesskey = u

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Darga nungu bayrandi…
    .accesskey = D
appmenu-help-not-deceptive =
    .label = Woo manti darga nungu…
    .accesskey = d

## More Tools

