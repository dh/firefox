# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Tekstcodering
    .accesskey = c

## Tools Menu

menu-addons-and-themes =
    .label = Add-ons en thema’s
    .accesskey = A

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Probleemoplossingsmodus…
    .accesskey = P
menu-help-exit-troubleshoot-mode =
    .label = Probleemoplossingsmodus uitschakelen
    .accesskey = c
menu-help-more-troubleshooting-info =
    .label = Meer probleemoplossingsinformatie
    .accesskey = M

## Mail Toolbar

toolbar-junk-button =
    .label = Ongewenst
    .tooltiptext = De geselecteerde berichten als ongewenst markeren
toolbar-not-junk-button =
    .label = Niet ongewenst
    .tooltiptext = De geselecteerde berichten als niet ongewenst markeren
toolbar-delete-button =
    .label = Verwijderen
    .tooltiptext = Geselecteerde berichten of map verwijderen
toolbar-undelete-button =
    .label = Verwijderen ongedaan maken
    .tooltiptext = Verwijdering van geselecteerde berichten ongedaan maken
