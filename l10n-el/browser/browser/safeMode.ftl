# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = Ασφαλής λειτουργία του { -brand-short-name }
    .style = max-width: 430px
start-safe-mode =
    .label = Εκκίνηση σε Ασφαλή λειτουργία
troubleshoot-mode-window =
    .title = Άνοιγμα του { -brand-short-name } σε λειτουργία επίλυσης προβλημάτων;
    .style = max-width: 470px
start-troubleshoot-mode =
    .label = Άνοιγμα
refresh-profile =
    .label = Ανανέωση του { -brand-short-name }
safe-mode-description = Η Ασφαλής λειτουργία είναι μια ειδική λειτουργία του { -brand-short-name } που μπορεί να χρησιμοποιηθεί για την αντιμετώπιση πιθανών προβλημάτων.
troubleshoot-mode-description = Χρησιμοποιήστε την ειδική λειτουργία του { -brand-short-name } για τη διάγνωση προβλημάτων. Οι επεκτάσεις και οι προσαρμογές σας θα απενεργοποιηθούν προσωρινά.
safe-mode-description-details = Τα πρόσθετα και οι προσαρμοσμένες ρυθμίσεις σας θα απενεργοποιηθούν προσωρινά και τα χαρακτηριστικά του { -brand-short-name } ενδέχεται να μην λειτουργούν όπως τώρα.
refresh-profile-instead = Μπορείτε επίσης να παραλείψετε την αντιμετώπιση προβλημάτων και να δοκιμάσετε την ανανέωση του { -brand-short-name }.
skip-troubleshoot-refresh-profile = Εναλλακτικά, μπορείτε επίσης να παραλείψετε την αντιμετώπιση προβλημάτων και να κάνετε ανανέωση του { -brand-short-name }.
# Shown on the safe mode dialog after multiple startup crashes.
auto-safe-mode-description = Το { -brand-short-name } τερματίστηκε απρόσμενα κατά την εκκίνηση. Αυτό μπορεί να προκλήθηκε από κάποιο πρόσθετο ή άλλα προβλήματα. Μπορείτε να προσπαθήσετε να επιλύσετε το πρόβλημα στην Ασφαλή λειτουργία.
