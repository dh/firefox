# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Variables:
# $count (Number) - Number of unread messages.
unread-messages-os-tooltip =
    { $count ->
        [one] 1 μη αναγνωσμένο μήνυμα
       *[other] { $count } μη αναγνωσμένα μηνύματα
    }
about-rights-notification-text = Το { -brand-short-name } είναι δωρεάν λογισμικό ανοικτού κώδικα, που δημιουργήθηκε από μια κοινότητα χιλιάδων ατόμων από όλο τον κόσμο.

## Toolbar

addons-and-themes-button =
    .label = Πρόσθετα και θέματα
    .tooltip = Διαχειριστείτε τα πρόσθετά σας

## Folder Pane

folder-pane-toolbar =
    .toolbarname = Γραμμή περιοχής φακέλων
    .accesskey = Γ
folder-pane-toolbar-options-button =
    .tooltiptext = Επιλογές προβολής φακέλων
folder-pane-header-label = Φάκελοι

## Folder Toolbar Header Popup

folder-toolbar-hide-toolbar-toolbarbutton =
    .label = Απόκρυψη γραμμής εργαλείων
    .accesskey = Α
show-all-folders-label =
    .label = Όλοι οι φάκελοι
    .accesskey = Ό
show-unread-folders-label =
    .label = Μη αναγνωσμένοι φάκελοι
    .accesskey = Μ
show-favorite-folders-label =
    .label = Αγαπημένοι φάκελοι
    .accesskey = π
show-smart-folders-label =
    .label = Ενοποιημένοι φάκελοι
    .accesskey = Ε
show-recent-folders-label =
    .label = Πρόσφατοι φάκελοι
    .accesskey = Π
folder-toolbar-toggle-folder-compact-view =
    .label = Συμπτυγμένη προβολή
    .accesskey = Σ

## AppMenu

# Since v89 we dropped the platforms distinction between Options or Preferences
# and consolidated everything with Preferences.
appmenu-preferences =
    .label = Προτιμήσεις
appmenu-addons-and-themes =
    .label = Πρόσθετα και θέματα
appmenu-help-enter-troubleshoot-mode =
    .label = Λειτουργία επίλυσης προβλημάτων…
appmenu-help-exit-troubleshoot-mode =
    .label = Απενεργοποίηση λειτουργίας επίλυσης προβλημάτων
appmenu-help-more-troubleshooting-info =
    .label = Πληροφορίες επίλυσης προβλημάτων
