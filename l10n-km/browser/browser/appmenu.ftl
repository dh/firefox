# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = ទាញយកបច្ចុប្បន្នភាព { -brand-shorter-name }
appmenuitem-customize-mode =
    .label = ប្ដូរ​តាម​តម្រូវ​ការ…

## Zoom Controls

appmenuitem-new-window =
    .label = បង្អួច​​​ថ្មី
appmenuitem-new-private-window =
    .label = បង្អួច​ឯកជន​ថ្មី

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = ពង្រីក
appmenuitem-zoom-reduce =
    .label = បង្រួម
appmenuitem-fullscreen =
    .label = អេក្រង់​ពេញ

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = ធ្វើ​សមកាលកម្ម​ឥឡូវ
appmenuitem-save-page =
    .label = រក្សា​ទុក​ទំព័រជា...

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = អំពី { -brand-shorter-name }
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = ព័ត៌មាន​អំពី​ដំណោះស្រាយ​បញ្ហា
    .accesskey = T
appmenu-help-taskmanager =
    .label = កម្មវិធី​គ្រប់គ្រង​ភារកិច្ច
appmenu-help-report-site-issue =
    .label = រាយការណ៍​បញ្ហា​គេហទំព័រ…
appmenu-help-feedback-page =
    .label = ដាក់​ស្នើ​មតិកែលម្អ…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = ចាប់ផ្ដើម​ឡើងវិញ​ដោយ​បិទ​ដំណើរការកម្មវិធី​បន្ថែម…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = ចាប់ផ្ដើម​ឡើងវិញ​ដោយ​បើកដំណើរការ​​កម្មវិធី​បន្ថែម
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = រាយការណ៍​អំពី​វេបសាយ​បញ្ឆោត…
    .accesskey = D
appmenu-help-not-deceptive =
    .label = នេះ​មិនមែន​ជា​វេបសាយ​បញ្ឆោត​ទេ…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = កម្មវិធី​គ្រប់គ្រង​ភារកិច្ច
