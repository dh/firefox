# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Change Password dialog

change-device-password-window =
    .title = Cambéu de la contraseña
# Variables:
# $tokenName (String) - Security device of the change password dialog
change-password-token = Preséu de seguranza: { $tokenName }
change-password-old = Contraseña actual:
change-password-new = Contraseña nueva:

## Reset Password dialog

pippki-failed-pw-change = Nun ye posible camudar la contraseña

## Reset Primary Password dialog


## Downloading cert dialog


## Client Authorization Ask dialog

client-auth-site-description = Esti sitiu solicitó que t'indentifiques con un certificáu:

## Set password (p12) dialog


## Protected Auth dialog

