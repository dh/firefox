# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

newsletter-thanks-title = ¡Gracies!
footer-learn-more-link = Lleer más
features-learn-more = Lleer más
features-inspector-title = Inspeutor
features-console-title = Consola
features-debugger-title = Depurador
features-network-title = Rede
features-storage-title = Almacenamientu
features-responsive-title = Mou de diseñu responsivu
features-visual-editing-title = Edición visual
features-performance-title = Rindimientu
features-memory-title = Memoria
newsletter-error-unknown = Asocedió un fallu inesperáu.
