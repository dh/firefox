# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Localization for Developer Tools options


## Default Developer Tools section

# The label for the heading of the radiobox corresponding to the theme
options-select-dev-tools-theme-label = Estilos

## Inspector section

# The heading
options-context-inspector = Inspeutor
# The label for the checkbox option to show user agent styles
options-show-user-agent-styles-label = Amosar los estilos del restolador

## "Default Color Unit" options for the Inspector


## Style Editor section

# The heading
options-styleeditor-label = Editor d'estilos

## Screenshot section


## Editor section

# The heading
options-sourceeditor-label = Preferencies del editor

## Advanced section

# The heading (this item is also used in perftools.ftl)
options-context-advanced-settings = Axustes avanzaos
