# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The question portion of the following message should have the <strong> and </strong> tags surrounding it.
default-browser-notification-message = <strong>¿Afitar { -brand-short-name } como'l restolador predetermináu?</strong> Consigui un restolar rápidu, seguru y priváu siempres qu'uses la web.
default-browser-notification-button =
    .label = Predeterminar
    .accesskey = P
