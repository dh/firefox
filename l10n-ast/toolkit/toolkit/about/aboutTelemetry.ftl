# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-telemetry-archive-ping-header = Ping
about-telemetry-option-group-today = Güei
about-telemetry-option-group-yesterday = Ayerí
about-telemetry-previous-ping = <<
about-telemetry-next-ping = >>
about-telemetry-home-section = Aniciu
about-telemetry-general-data-section = Datos xenerales
about-telemetry-events-section = Eventos
# string used as a placeholder for the search field
# More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# Variables:
#   $selectedTitle (String): the section name from the structure of the ping.
about-telemetry-filter-placeholder =
    .placeholder = Atopar en { $selectedTitle }
about-telemetry-keys-header = Propiedá
about-telemetry-names-header = Nome
about-telemetry-values-header = Valor
