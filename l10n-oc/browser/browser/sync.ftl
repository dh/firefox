# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-toolbar-sync-syncing =
    .label = Sincronizacion…
fxa-toolbar-sync-syncing-tabs =
    .label = Sincronizacion dels onglets…
fxa-toolbar-sync-syncing2 = Sincronizacion…
sync-disconnect-dialog-title = Se desconnectar de { -sync-brand-short-name } ?
sync-disconnect-dialog-title2 = Se desconnectar ?
sync-disconnect-dialog-body = { -brand-product-name } quitarà de sincronizar vòstre compte mas escafarà pas las donadas de navegacion d’aqueste periferic.
fxa-disconnect-dialog-title = Se desconnectar de { -brand-product-name } ?
fxa-disconnect-dialog-body = { -brand-product-name } se desconnectarà del compte vòstre mas escafarà pas las donadas de navegacion d’aqueste periferic.
sync-disconnect-dialog-button = Se desconectar
fxa-signout-dialog-heading = Se desconnectar de{ -fxaccount-brand-name } ?
fxa-signout-dialog2-title = Se desconnectar de { -fxaccount-brand-name } ?
fxa-signout-dialog-body = Las donadas sincronizadas demoraràn dins vòstre compte.
fxa-signout-checkbox =
    .label = Suprimir las donadas d’aqueste periferic (identificants, senhals, istoric, marcapaginas, etc.).
fxa-signout-dialog =
    .title = Desconnexion de { -fxaccount-brand-name }
    .style = min-width: 375px;
    .buttonlabelaccept = Desconnexion
fxa-signout-dialog2-button = Se desconnectar
fxa-signout-dialog2-checkbox = Suprimir las donadas d’aqueste periferic (senhals, istoric, marcapaginas, etc.).
fxa-menu-sync-settings =
    .label = Paramètres de sincronizacion
fxa-menu-turn-on-sync =
    .value = Activar la sincronizacion
fxa-menu-turn-on-sync-default = Activar la sincronizacion
fxa-menu-connect-another-device =
    .label = Connectar un autre periferic…
fxa-menu-sign-out =
    .label = Se desconnectar…
