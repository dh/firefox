# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = விருப்பமை…

## Zoom Controls

appmenuitem-new-window =
    .label = புதிய சாளரம்
appmenuitem-new-private-window =
    .label = புதிய கமுக்க சாளரம்

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = முழுத்திரை

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = இப்போது ஒத்திசை
appmenuitem-save-page =
    .label = இவ்வாறு சேமி…

## What's New panel in App menu.

whatsnew-panel-header = புதியவை என்ன

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = { -brand-shorter-name } பற்றி
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = பிழைத்திருத்தல் தகவல்
    .accesskey = T
appmenu-help-report-site-issue =
    .label = தள சிக்கலை தெரிவி…
appmenu-help-feedback-page =
    .label = கருத்துக்களைச் சமர்ப்பி…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = நிரலை நீக்கியபின் மீட்துவக்கு…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = நிரலை நீக்கியபின் மீட்துவக்கு…
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = ஏமாற்று தளத்தைப் புகார் செய்…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = இது ஓர் ஏமாற்று தளம் அல்ல
    .accesskey = d

## More Tools

