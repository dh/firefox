# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

printui-title = அச்சிடு
# Dialog title to prompt the user for a filename to save print to PDF.
printui-save-to-pdf-title = இப்படி சேமி
# Variables
# $sheetCount (integer) - Number of paper sheets
printui-sheets-count =
    { $sheetCount ->
        [one] { $sheetCount } தாள்
       *[other] { $sheetCount } தாள்கள்
    }

## Paper sizes that may be supported by the Save to PDF destination:


## Error messages shown when a user has an invalid input

