# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-masonry2 =
    .label = CSS: Masonry Layout
experimental-features-css-masonry-description = Tio ĉi aktivigas la subtenon de la eksperimenta trajto CSS Masonry Layout. Vidu la <a data-l10n-name="explainer">klarigon</a> por altnivela priskribo de la trajto. Se vi volas opinii, bonvolu komenti en <a data-l10n-name="w3c-issue">tiu ĉi problemo de GitHub</a> aŭ en <a data-l10n-name="bug">tiu ĉi eraro</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-gpu2 =
    .label = Web API: WebGPU
experimental-features-web-gpu-description2 = Tiu nova API provizas malaltnivelan subtenon por kalkuloj kaj montro de grafikaĵoj faritaj de <a data-l10n-name="wikipedia">grafika procesoro (GPU)</a> en la aparato aŭ komputilo de la uzanto. La <a data-l10n-name="spec">specifo</a> estas ankoraŭ prilaborata. Vizitu la <a data-l10n-name="bugzilla">problemon 1602129</a> por havi pli da informo.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Media: AVIF
experimental-features-media-avif-description = Se tiu ĉi trajto estas aktiva, { -brand-short-name } subtenas la bildan dosierformon AV1 (AVIF). Tiu ĉi dosierformo uzas la kapablojn de la videokompaktigaj algoritmoj de AV1 por redukti la grandon de bildo. Vizitu la <a data-l10n-name="bugzilla">problemon 1443863</a> por havi pli da informo.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-inputmode =
    .label = Web API: inputmode
# "inputmode" and "contenteditable" are technical terms and shouldn't be translated.
experimental-features-web-api-inputmode-description = Nia realigo de la <a data-l10n-name="mdn-inputmode">inputmode</a> ĝenerala atributo estis ĝisdatigita laŭ <a data-l10n-name="whatwg">la specifo de WHATWG</a>, sed restas ankoraŭ aliaj ŝanĝoj, kiel la disponigo en modifebla enhavo kreita de contenteditable. Vizitu la <a data-l10n-name="bugzilla">problemon 1205133</a> por havi pli da informo.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-link-preload =
    .label = Web API: <link rel="preload">
# Do not translate "rel", "preload" or "link" here, as they are all HTML spec
# values that do not get translated.
experimental-features-web-api-link-preload-description = La atributo <a data-l10n-name="rel">rel</a> kun la valoro <code>"preload"</code> en elemento <a data-l10n-name="link">&lt;link&gt;</a> havas la celon plibonigi la efikecon per pli frua ŝargado de rimedoj dum la vivo de la paĝo, kio signifas ke ili estos pli frue disponeblaj kaj malpli probable blokos la montron de la paĝo. Legu <a data-l10n-name="readmore">“Antaŭŝargado de enhavo per <code>rel="preload"</code>”</a> aŭ vizitu la <a data-l10n-name="bugzilla">problemon 1583604</a> por havi pli da informo.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-focus-visible =
    .label = CSS: Pseudo-class: :focus-visible
experimental-features-css-focus-visible-description = Tio ĉi permesas apliki fokusajn stilojn al elementoj kiel butonoj kaj formularaj elementoj, nur kiam ili ricevas la fokuson per klavaro (ekzemple dum uzo de tabklavo inter elementoj), sed ne kiam ili ricevas la fokuson per musalklako aŭ per iu alia aparato. Vizitu la <a data-l10n-name="bugzilla">problemon 1617600</a> por havi pli da informo.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-beforeinput =
    .label = Web API: beforeinput Event
# The terms "beforeinput", "input", "textarea", and "contenteditable" are technical terms
# and shouldn't be translated.
experimental-features-web-api-beforeinput-description = La ĝenerala evento <a data-l10n-name="mdn-beforeinput">beforeinput</a> iĝas aktiva en elementoj <a data-l10n-name="mdn-input">&lt;input&gt;</a> kaj <a data-l10n-name="mdn-textarea">&lt;textarea&gt;</a>, aŭ en iu ajn elemento kies atributo <a data-l10n-name="mdn-contenteditable">contenteditable</a> estas aktiva, tuj antaŭ la ŝanĝiĝo de valoro de la elemento. Tiu evento permesas al teksaĵaj programoj superregi la norman konduton de la retumilo rilate al interago de uzanto, ezkemple, programoj teksaĵaj povas nuligi la tajpadon de kelkaj signoj, aŭ povas modifi la stilon de algluita teksto tiel ke kongruas kun aprobita listo de stiloj.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-constructable-stylesheets =
    .label = CSS: Constructable Stylesheets
experimental-features-css-constructable-stylesheets-description = La aldono de konstruilo al la fasado de <a data-l10n-name="mdn-cssstylesheet">CSSStyleSheet</a>, kune kun pluraj aliaj rilataj ŝanĝoj, ebligas la senperan kreadon de novaj stilfolioj sen devi aldoni la stilfolion al la HTML kodo. Tio ĉi faciligas multe la kreadon de reuzeblaj stilfolioj por uzo kun <a data-l10n-name="mdn-shadowdom">Shadow DOM</a>. Vizitu la <a data-l10n-name="bugzilla">problemon 1520690</a> por havi pli da informo.
experimental-features-devtools-color-scheme-simulation =
    .label = Iloj por programistoj: imitilo de kolora skemo
experimental-features-devtools-color-scheme-simulation-description = Tio ĉi aldonas eblon imiti malsamajn kolorajn skemojn, kio permesas al vi testi aŭdvidaĵajn petojn <a data-l10n-name="mdn-preferscolorscheme">@prefers-color-scheme</a>. Per tiu ĉi aŭdvidaĵa peto via stilfolio povas respondi al la preferoj de la uzanto rilate al la heleco de la aspekto. Tiu ĉi trajto permesas al vi testi vian kodon sen devi ŝanĝi agordojn en via retumilo (aŭ mastruma sistemo, se la retumilo sekvas la sisteman norman koloran skemon). Vizitu la <a data-l10n-name="bugzilla1">problemon 1550804</a> kaj la <a data-l10n-name="bugzilla2">problemon 1137699</a> por havi pli da informo.
experimental-features-devtools-execution-context-selector =
    .label = Iloj por programistoj: elektilo de rulada kunteksto
experimental-features-devtools-execution-context-selector-description = Tiu ĉi trajto montras butonon en la ordonlinio de la konzolo, kiu permesas al vi ŝanĝi la kuntekston en kiu la esprimo tajpita de vi estos taksita. Vizitu la <a data-l10n-name="bugzilla1">problemon 1605154</a> kaj la <a data-l10n-name="bugzilla2">problemon 1605153</a> por havi pli da informo.
experimental-features-devtools-compatibility-panel =
    .label = Iloj por programistoj: kongrueca panelo
experimental-features-devtools-compatibility-panel-description = Flanka panelo por la inspektilo de paĝo, kiu montras detalajn informojn pri la interretumila kongrueco de via programo. Vizitu la <a data-l10n-name="bugzilla">problemon 1584464</a> por havi pli da informoj.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Kuketoj: SameSite=Lax estas la normo
experimental-features-cookie-samesite-lax-by-default2-description = Norme pritrakti kuketojn kiel “SameSite=Lax” se neniu atributo “SameSite” estas specifita. Programistoj devas elekti la nunan faktan koduton, senliman uzon, per malimplicita difino de “SameSite=None”.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-none-requires-secure2 =
    .label = Kuketoj: SameSite=None postulas la atributon secure
experimental-features-cookie-samesite-none-requires-secure2-description = Kuketoj kun “SameSite=None” postulas la atributon secure. Tiu ĉi trajto postulas “Kuketoj: SameSite=Lax estas la normo”.
# about:home should be kept in English, as it refers to the the URI for
# the internal default home page.
experimental-features-abouthome-startup-cache =
    .label = about:home komenca staplo
experimental-features-abouthome-startup-cache-description = Staplo por la komenca dokumento about:home, kiu estas norme ŝargita komence. La celo de tiu ĉi staplo estas plibonigi la efikecon de starto.
experimental-features-print-preview-tab-modal =
    .label = Refasonado de la antaŭvido de presado
experimental-features-print-preview-tab-modal-description = Tio ĉi enkondukas la refasonitan antaŭvidon de presado kaj disponigas la antaŭvidon de presado en macOS. Tio povus eventuale misfunkcii kaj ĝi ne inkluzivas ĉiujn agordojn presajn. Por aliri ĉiuj tiujn agordojn, elektu "Presi per sistema dialogo…” ene de la presa panelo.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-cookie-samesite-schemeful =
    .label = Cookies: Schemeful SameSite
experimental-features-cookie-samesite-schemeful-description = Pritrakti kuketojn el la sama nomrego, sed kun malsamaj skemoj (ekzemple http://example.com kaj https://example.com) kiel interretejajn anstataŭ samretejajn. Tio plibonigas sekurecon, sed povus eventuale misfukciigi aferojn.
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support =
    .label = Iloj por programistoj: senerarigo de Service worker
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support-description = Tio ĉi aktivigas eksperimentan subtenon de Service workers en la erarserĉila panelo. Tiu ĉi trajto povas malrapidigi la ilojn por programistoj kaj uzi pli da memoro.
# WebRTC global mute toggle controls
experimental-features-webrtc-global-mute-toggles =
    .label = Ŝalti/malŝalti sonon WebRTC ĉie
experimental-features-webrtc-global-mute-toggles-description = Aldoni regilojn al la ĉiea indikilo pri dividi de WebRTC, kiu permesas al uzantoj malaktivigi siajn mikrofonojn kaj fimilojn ĉie.
# JS JIT Warp project
experimental-features-js-warp =
    .label = JavaScript JIT: Warp
experimental-features-js-warp-description = Aktivigi Warp, kiu estas projekto por plibonigi la efikecon kaj memoruzon de JavaScript.
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (reteja izolado)
experimental-features-fission-description = Fission (reteja izolado) estas eksperimenta trajto en { -brand-short-name } kiu provizas aldonan defendan tavolon kontraŭ sekurecaj problemoj. Fission izolas ĉiun retejon, kaj do malicaj paĝoj ne povas aliri informojn de aliaj vizitataj paĝoj. Tiu ĉi estas tre grava kerna arĥitektura ŝanĝo en { -brand-short-name } kaj ni dankas vian testadon kaj la raporton de iu ajn problemo, kiun vi renkontos. Por havi pli da informo, vizitu <a data-l10n-name="wiki">la vikion</a>.
# Support for having multiple Picture-in-Picture windows open simultaneously
experimental-features-multi-pip =
    .label = Subteno por pluraj inkrustitaj filmetoj
experimental-features-multi-pip-description = Eksperimenta subteno por permesi la samtempan malfermon de pluraj inkrustitaj filmetoj.
experimental-features-http3 =
    .label = Protokolo HTTP/3
experimental-features-http3-description = Eksperimenta subteno de la protokolo HTTP/3.
# Search during IME
experimental-features-ime-search =
    .label = Adresa strio: montri rezultojn dum komponado IME
experimental-features-ime-search-description = IME (redaktilo de eniga metodo) estas ilo, kiu permesas al vi, pere de normala klavaro, tajpi malsimplajn signojn, kiel la uzatajn en la skribataj lingvoj de orienta Azio aŭ Barato. Se vi ŝaltas tiun ĉi eksperimenton, la adresa strio restos malfermita dum vi uzas IME por tajpi ion. Notu ke IME povus montri panelon kiu kovras la rezultojn de la adresa strio, tial tiu ĉi prefero estas sugestita por la IME, kiuj ne uzas tiajn panelojn.
