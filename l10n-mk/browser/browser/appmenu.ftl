# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Прилагоди…

## Zoom Controls

appmenuitem-new-window =
    .label = Нов прозорец
appmenuitem-new-private-window =
    .label = Нов приватен прозорец

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = На цел екран

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Синхронизирај сега
appmenuitem-save-page =
    .label = Сними страница како…

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = За { -brand-shorter-name }
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = Информации за проблеми
    .accesskey = И
appmenu-help-feedback-page =
    .label = Испрати коментар…
    .accesskey = с

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Рестартирај со исклучени додатоци…
    .accesskey = Р
appmenu-help-safe-mode-with-addons =
    .label = Рестрартирај со овозможени додатоци
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.


## More Tools

