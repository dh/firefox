# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Saincheap…

## Zoom Controls

appmenuitem-new-window =
    .label = Fuinneog Nua
appmenuitem-new-private-window =
    .label = Fuinneog Nua Phríobháideach

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Zúmáil isteach
appmenuitem-zoom-reduce =
    .label = Zúmáil amach
appmenuitem-fullscreen =
    .label = Lánscáileán

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sioncronaigh Anois
appmenuitem-save-page =
    .label = Sábháil an Leathanach Mar…

## What's New panel in App menu.

whatsnew-panel-header = Gnéithe Nua

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Maidir le { -brand-shorter-name }
    .accesskey = M
appmenu-help-troubleshooting-info =
    .label = Fabhtcheartú
    .accesskey = t
appmenu-help-taskmanager =
    .label = Bainisteoir Tascanna
appmenu-help-report-site-issue =
    .label = Tuairiscigh Fadhb le Suíomh…
appmenu-help-feedback-page =
    .label = Seol Aiseolas Chugainn…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Atosaigh gan aon bhreiseáin…
    .accesskey = A
appmenu-help-safe-mode-with-addons =
    .label = Atosaigh leis na Breiseáin ar siúl…
    .accesskey = A

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Tuairiscigh suíomh cealgach…
    .accesskey = c
appmenu-help-not-deceptive =
    .label = Ní suíomh cealgach é seo…
    .accesskey = c

## More Tools

appmenu-taskmanager =
    .label = Bainisteoir Tascanna
