# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-editable-item-privacy-icon-private =
    .alt = 隐私：私人事件
calendar-editable-item-privacy-icon-confidential =
    .alt = 隐私：只显示时间和日期
