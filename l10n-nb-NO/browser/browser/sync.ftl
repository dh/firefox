# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-toolbar-sync-syncing =
    .label = Synkroniserer…
fxa-toolbar-sync-syncing-tabs =
    .label = Synkroniserer faner…
fxa-toolbar-sync-syncing2 = Synkroniserer…
sync-disconnect-dialog-title = Koble fra { -sync-brand-short-name }?
sync-disconnect-dialog-title2 = Koble fra?
sync-disconnect-dialog-body = { -brand-product-name } vil slutte å synkronisere kontoen din, men sletter ikke nettleserdata på denne enheten.
fxa-disconnect-dialog-title = Koble fra { -brand-product-name }?
fxa-disconnect-dialog-body = { -brand-product-name } vil koble fra denne kontoen, men sletter ikke nettleserdata på denne enheten.
sync-disconnect-dialog-button = Koble fra
fxa-signout-dialog-heading = Logge ut av { -fxaccount-brand-name }?
fxa-signout-dialog2-title = Logg ut av { -fxaccount-brand-name }?
fxa-signout-dialog-body = Synkroniserte data forblir på kontoen din.
fxa-signout-checkbox =
    .label = Slett data fra denne enheten (innlogginger, passord, historikk, bokmerker osv.).
fxa-signout-dialog =
    .title = Logge ut av { -fxaccount-brand-name }?
    .style = min-width: 375px;
    .buttonlabelaccept = Logg ut
fxa-signout-dialog2-button = Logg ut
fxa-signout-dialog2-checkbox = Slett data fra denne enheten (passord, historikk, bokmerker osv.).
fxa-menu-sync-settings =
    .label = Innstillinger for synkronisering
fxa-menu-turn-on-sync =
    .value = Slå på synkronisering
fxa-menu-turn-on-sync-default = Slå på synkronisering
fxa-menu-connect-another-device =
    .label = Koble til en annen enhet…
fxa-menu-sign-out =
    .label = Logg ut…
