# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Page title
about-processes-title = ตัวจัดการกระบวนการ
# The Actions column
about-processes-column-action =
    .title = การกระทำ

## Tooltips

about-processes-shutdown-process =
    .title = เลิกโหลดแท็บและหยุดการทำงานของกระบวนการ
about-processes-shutdown-tab =
    .title = ปิดแท็บ

## Column headers

about-processes-column-name = ชื่อ
about-processes-column-memory-resident = หน่วยความจำ
about-processes-column-cpu-total = CPU

## Process names
## Variables:
##    $pid (String) The process id of this process, assigned by the OS.
##    $origin (String) The domain name for this process.
##    $type (String) The raw type for this process. Used for unknown processes.

about-processes-browser-process-name = { -brand-short-name } (กระบวนการ { $pid })
about-processes-web-process-name = เว็บ (กระบวนการ { $pid }, ใช้ร่วมกัน)
about-processes-web-isolated-process-name = เว็บ (กระบวนการ { $pid }) สำหรับ { $origin }
about-processes-web-large-allocation = เว็บ (กระบวนการ { $pid }, ขนาดใหญ่) สำหรับ { $origin }
about-processes-with-coop-coep-process-name = เว็บ (กระบวนการ { $pid }, แยกแบบข้ามที่มา) สำหรับ { $origin }
about-processes-file-process-name = ไฟล์ (กระบวนการ { $pid })
about-processes-extension-process-name = ส่วนขยาย (กระบวนการ { $pid })
about-processes-privilegedabout-process-name = เกี่ยวกับ (กระบวนการ { $pid })
about-processes-plugin-process-name = ปลั๊กอิน (กระบวนการ { $pid })
about-processes-privilegedmozilla-process-name = เว็บ (กระบวนการ { $pid }) สำหรับไซต์ { -vendor-short-name }
about-processes-gmp-plugin-process-name = ปลั๊กอินสื่อของ Gecko (กระบวนการ { $pid })
about-processes-gpu-process-name = GPU (กระบวนการ { $pid })
about-processes-vr-process-name = VR (กระบวนการ { $pid })
about-processes-rdd-process-name = ตัวถอดรหัสข้อมูล (กระบวนการ { $pid })
about-processes-socket-process-name = เครือข่าย (กระบวนการ { $pid })
about-processes-remote-sandbox-broker-process-name = ตัวกลาง Sandbox ระยะไกล (กระบวนการ { $pid })
about-processes-fork-server-process-name = เซิร์ฟเวอร์ Fork (กระบวนการ { $pid })
about-processes-preallocated-process-name = จัดสรรล่วงหน้า (กระบวนการ { $pid })
about-processes-unknown-process-name = อื่นๆ ({ $type }, กระบวนการ { $pid })
# Process
# Variables:
#   $name (String) The name assigned to the process.
#   $pid (String) The process id of this process, assigned by the OS.
about-processes-process-name = กระบวนการ { $pid }: { $name }

## Details within processes

# Single-line summary of threads
# Variables:
#    $number (Number) The number of threads in the process. Typically larger
#                     than 30. We don't expect to ever have processes with less
#                     than 5 threads.
about-processes-thread-summary = เธรด ({ $number })
# Thread details
# Variables:
#   $name (String) The name assigned to the thread.
#   $tid (String) The thread id of this thread, assigned by the OS.
about-processes-thread-name = เธรด { $tid }: { $name }
# Tab
# Variables:
#   $name (String) The name of the tab (typically the title of the page, might be the url while the page is loading).
about-processes-tab-name = แท็บ: { $name }
about-processes-preloaded-tab = แท็บใหม่ที่โหลดไว้ล่วงหน้า
# Single subframe
# Variables:
#   $url (String) The full url of this subframe.
about-processes-frame-name-one = เฟรมย่อย: { $url }
# Group of subframes
# Variables:
#   $number (Number) The number of subframes in this group. Always ≥ 1.
#   $shortUrl (String) The shared prefix for the subframes in the group.
about-processes-frame-name-many = เฟรมย่อย ({ $number }): { $shortUrl }

## Displaying CPU (percentage and total)
## Variables:
##    $percent (Number) The percentage of CPU used by the process or thread.
##                      Always > 0, generally <= 200.
##    $total (Number) The amount of time used by the process or thread since
##                    its start.
##    $unit (String) The unit in which to display $total. See the definitions
##                   of `duration-unit-*`.

# Common case.
about-processes-cpu-user-and-kernel = { NUMBER($percent, maximumSignificantDigits: 2, style: "percent") } ({ NUMBER($total, maximumFractionDigits: 0) }{ $unit })
# Special case: data is not available yet.
about-processes-cpu-user-and-kernel-not-ready = (กำลังวัด)
# Special case: process or thread is currently idle.
about-processes-cpu-user-and-kernel-idle = ไม่ได้ใช้งาน ({ NUMBER($total, maximumFractionDigits: 2) }{ $unit })

## Displaying Memory (total and delta)
## Variables:
##    $total (Number) The amount of memory currently used by the process.
##    $totalUnit (String) The unit in which to display $total. See the definitions
##                        of `memory-unit-*`.
##    $delta (Number) The absolute value of the amount of memory added recently.
##    $deltaSign (String) Either "+" if the amount of memory has increased
##                        or "-" if it has decreased.
##    $deltaUnit (String) The unit in which to display $delta. See the definitions
##                        of `memory-unit-*`.

# Common case.
about-processes-total-memory-size = { NUMBER($total, maximumFractionDigits: 0) }{ $totalUnit } ({ $deltaSign }{ NUMBER($delta, maximumFractionDigits: 0) }{ $deltaUnit })
# Special case: no change.
about-processes-total-memory-size-no-change = { NUMBER($total, maximumFractionDigits: 0) }{ $totalUnit }

## Duration units

duration-unit-ns = ns
duration-unit-us = µs
duration-unit-ms = ms
duration-unit-s = วินาที
duration-unit-m = นาที
duration-unit-h = ชม.
duration-unit-d = วัน

## Memory units

memory-unit-B = B
memory-unit-KB = KB
memory-unit-MB = MB
memory-unit-GB = GB
memory-unit-TB = TB
memory-unit-PB = PB
memory-unit-EB = EB
