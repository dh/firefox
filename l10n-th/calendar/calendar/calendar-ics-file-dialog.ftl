# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-ics-file-window-2 =
    .title = นำเข้าเหตุการณ์และงานในปฏิทิน
calendar-ics-file-dialog-import-event-button-label = นำเข้าเหตุการณ์
calendar-ics-file-dialog-import-task-button-label = นำเข้างาน
calendar-ics-file-dialog-2 =
    .buttonlabelaccept = นำเข้าทั้งหมด
calendar-ics-file-accept-button-ok-label = ตกลง
calendar-ics-file-cancel-button-close-label = ปิด
calendar-ics-file-dialog-message-2 = นำเข้าจากไฟล์…
calendar-ics-file-dialog-calendar-menu-label = นำเข้าสู่ปฏิทิน:
calendar-ics-file-dialog-items-loading-message =
    .value = กำลังโหลดรายการ…
calendar-ics-file-dialog-progress-message = กำลังนำเข้า…
calendar-ics-file-import-success = นำเข้าเรียบร้อย!
calendar-ics-file-import-error = เกิดข้อผิดพลาดและการนำเข้าล้มเหลว
calendar-ics-file-import-complete = การนำเข้าเสร็จสมบูรณ์
calendar-ics-file-import-duplicates =
    { $duplicatesCount ->
       *[other] { $duplicatesCount } รายการถูกเพิกเฉยเนื่องจากมีอยู่ในปฏิทินปลายทางแล้ว
    }
calendar-ics-file-import-errors =
    { $errorsCount ->
       *[other] ไม่สามารถนำเข้า { $errorsCount } รายการได้ ตรวจสอบคอนโซลข้อผิดพลาดสำหรับรายละเอียด
    }
calendar-ics-file-dialog-no-calendars = ไม่มีปฏิทินที่สามารถนำเข้าเหตุการณ์หรืองานได้
