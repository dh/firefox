# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-one-recipient-status-title =
    .title = ความปลอดภัยของข้อความ OpenPGP
openpgp-one-recipient-status-status =
    .label = สถานะ
openpgp-one-recipient-status-key-id =
    .label = ID คีย์
openpgp-one-recipient-status-created-date =
    .label = สร้างเมื่อ
openpgp-one-recipient-status-expires-date =
    .label = หมดอายุ
