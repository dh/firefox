# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = নিজের পছন্দানুযায়ী নির্বাচন…

## Zoom Controls

appmenuitem-new-window =
    .label = নতুন উইন্ডো
appmenuitem-new-private-window =
    .label = নতুন ব্যক্তিগত উইন্ডো

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = বড় করুন
appmenuitem-zoom-reduce =
    .label = ছোট করুন
appmenuitem-fullscreen =
    .label = পূর্ণ পর্দাজুড়ে প্রদর্শন

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = এখনই সিঙ্ক করুন
appmenuitem-save-page =
    .label = পাতা নতুনভাবে সংরক্ষণ…

## What's New panel in App menu.

whatsnew-panel-header = নতুন কি আছে

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = About { -brand-shorter-name }
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = সমাধান করার তথ্য
    .accesskey = T
appmenu-help-taskmanager =
    .label = কাজ ব্যবস্থাপক
appmenu-help-report-site-issue =
    .label = সাইটের সমস্যা রিপোর্ট করুন…
appmenu-help-feedback-page =
    .label = মন্তব্য প্রদান…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = নিস্ক্রিয় অ্যাড-অনসহ পুনরায় শুরু…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = অ্যাড-অন সক্রিয় করে পুনরায় চালু করুন
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = ক্ষতিকারক সাইট হিসেবে রিপোর্ট করুন…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = এটি কোন ক্ষতিকারক সাইট না…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = কাজ ব্যবস্থাপক
