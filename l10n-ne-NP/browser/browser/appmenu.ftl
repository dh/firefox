# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = अनुकुलित बनाउनुहोस्...

## Zoom Controls

appmenuitem-new-window =
    .label = नयाँ सञ्झ्याल
appmenuitem-new-private-window =
    .label = नयाँ निजी सञ्झ्याल

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = पूरा पर्दा

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = अहिले सिङ्क गर्नुहोस्
appmenuitem-save-page =
    .label = यस रूपमा पृष्ठ सङ्ग्रह गर्नुहोस्...

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = { -brand-shorter-name } को बारेमा
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = समस्या समाधानको सूचना
    .accesskey = T
appmenu-help-report-site-issue =
    .label = साइटको समस्या दर्ता गर्नुहोस्‌…
appmenu-help-feedback-page =
    .label = प्रतिक्रिया दिनुहोस्…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = एडअनहरू अक्षम पारेर पुनःसुरु गर्नुहोस् …
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = एडअनहरू सक्षम पारेर पुनःसुरु गर्नुहोस्
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = आक्रामक साइट भएको प्रतिवेदन दिनुहोस्…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = यो भ्रामक साइट होइन…
    .accesskey = d

## More Tools

