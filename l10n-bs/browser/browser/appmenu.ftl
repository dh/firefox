# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = Preuzimam { -brand-shorter-name } nadogradnju
appmenuitem-protection-dashboard-title = Dashboard zaštite
appmenuitem-customize-mode =
    .label = Prilagođavanje…

## Zoom Controls

appmenuitem-new-window =
    .label = Novi prozor
appmenuitem-new-private-window =
    .label = Novi privatni prozor

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Uvećaj
appmenuitem-zoom-reduce =
    .label = Umanji
appmenuitem-fullscreen =
    .label = Prikaz preko cijelog ekrana

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sinhronizuj sada
appmenuitem-save-page =
    .label = Spasi stranicu kao…

## What's New panel in App menu.

whatsnew-panel-header = Šta je novo
# Checkbox displayed at the bottom of the What's New panel, allowing users to
# enable/disable What's New notifications.
whatsnew-panel-footer-checkbox =
    .label = Obavijesti o novim mogućnostima
    .accesskey = f

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = O { -brand-shorter-name }u
    .accesskey = O
appmenu-help-troubleshooting-info =
    .label = Informacije za rješavanje problema
    .accesskey = I
appmenu-help-taskmanager =
    .label = Task Manager
appmenu-help-report-site-issue =
    .label = Prijavite problem sa stranicom…
appmenu-help-feedback-page =
    .label = Pošalji povratnu informaciju…
    .accesskey = P

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Restartuj sa onemogućenim add-onima…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = Restartuj sa omogućenim add-onima
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Prijavi obmanjujuću stranicu…
    .accesskey = o
appmenu-help-not-deceptive =
    .label = Ovo nije obmanjujuća stranica…
    .accesskey = o

## More Tools

appmenu-taskmanager =
    .label = Task Manager
