# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = मनपसंत करा…

## Zoom Controls

appmenuitem-new-window =
    .label = नवीन पटल
appmenuitem-new-private-window =
    .label = नवीन खाजगी पटल

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = मोठे करा
appmenuitem-zoom-reduce =
    .label = छोटे करा
appmenuitem-fullscreen =
    .label = पडदाभर

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = आत्ता सिंक करा
appmenuitem-save-page =
    .label = पृष्ठ असे साठवा…

## What's New panel in App menu.

whatsnew-panel-header = नवीन काय आहे

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = { -brand-shorter-name } विषयी
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = त्रुटीनिवारण माहिती
    .accesskey = T
appmenu-help-taskmanager =
    .label = कार्य व्यवस्थापक
appmenu-help-report-site-issue =
    .label = साईटची त्रुटी दाखल करा…
appmenu-help-feedback-page =
    .label = अभिप्राय सादर करा…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = ॲड-ऑन्स् बंद असल्यावर पुनः सुरू करा…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = ॲड-ऑन्स् सक्रीय असल्यावर पुनः सुरू करा
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = फसवी साईट कळवा…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = ही साईट फसवी नाही…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = कार्य व्यवस्थापक
