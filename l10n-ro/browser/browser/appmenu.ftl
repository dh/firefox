# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = Se descarcă actualizarea { -brand-shorter-name }
appmenuitem-protection-dashboard-title = Tablou de bord privind protecțiile
appmenuitem-customize-mode =
    .label = Personalizează…

## Zoom Controls

appmenuitem-new-window =
    .label = Fereastră nouă
appmenuitem-new-private-window =
    .label = Fereastră privată nouă

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Mărește
appmenuitem-zoom-reduce =
    .label = Micșorează
appmenuitem-fullscreen =
    .label = Ecran complet

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sincronizează acum
appmenuitem-save-page =
    .label = Salvează pagina ca…

## What's New panel in App menu.

whatsnew-panel-header = Ce este nou
# Checkbox displayed at the bottom of the What's New panel, allowing users to
# enable/disable What's New notifications.
whatsnew-panel-footer-checkbox =
    .label = Anunță-mă despre noi funcționalități
    .accesskey = f

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Despre { -brand-shorter-name }
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = Informații pentru depanare
    .accesskey = T
appmenu-help-taskmanager =
    .label = Manager de activități
appmenu-help-report-site-issue =
    .label = Raportează problemă cu site-ul…
appmenu-help-feedback-page =
    .label = Trimite feedback…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Repornește cu suplimentele dezactivate…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = Repornește cu suplimentele activate
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Raportează site-uri înșelătoare…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = Nu este un site înșelător…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = Manager de activități
