# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-ics-file-window-2 =
    .title = Kter tidyanin d twuriwin n uwitay
calendar-ics-file-dialog-import-event-button-label = Kter tadyant
calendar-ics-file-dialog-import-task-button-label = Tawuri nuktar
calendar-ics-file-dialog-2 =
    .buttonlabelaccept = Kter akk
calendar-ics-file-accept-button-ok-label = IH
calendar-ics-file-cancel-button-close-label = Mdel
calendar-ics-file-dialog-message-2 = Kter seg ufaylu:
calendar-ics-file-dialog-calendar-menu-label = Kter deg uwitay
calendar-ics-file-dialog-items-loading-message =
    .value = Asali n yiferdisen...
calendar-ics-file-dialog-progress-message = Akter...
calendar-ics-file-import-success = Yettwakter akken iwata!
calendar-ics-file-import-error = Tella-d tuccḍa, aktar ur yeddu ara.
calendar-ics-file-import-complete = Akter yemmed.
calendar-ics-file-dialog-no-calendars = Ula d yiwen uwitay ur yezmir ad yekter tidyanin d twuriwin.
