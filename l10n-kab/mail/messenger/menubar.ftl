# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Asettengel n uḍris
    .accesskey = s

## Mail Toolbar

toolbar-junk-button =
    .label = Aspam
    .tooltiptext = Creḍ iznan yettwafernen d ispamen
toolbar-not-junk-button =
    .label = Mačči d aspam
    .tooltiptext = Creḍ iznan yettwafernen mačči d ispamen
toolbar-delete-button =
    .label = Kkes
    .tooltiptext = Kkes iznan neɣ ikaramen yettwafernen
toolbar-undelete-button =
    .label = Sefsex tukksa
    .tooltiptext = Sefsex tukksa n yiznan yettwafernen
