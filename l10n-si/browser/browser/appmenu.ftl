# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = රිසිකරණය...

## Zoom Controls

appmenuitem-new-window =
    .label = නව කවුළුවක්
appmenuitem-new-private-window =
    .label = නව පුද්ගලික කවුළුව

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = පූර්ණ තිරය

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = දැන් සම්මුහුර්ත කරන්න
appmenuitem-save-page =
    .label = පිටුව සුරකින්න...

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = { -brand-shorter-name } පිළිබඳ
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = දෝෂ සෙවීමේ තොරතුරු
    .accesskey = T
appmenu-help-report-site-issue =
    .label = අඩවියේ දෝශය වාර්ථා කරන්න…
appmenu-help-feedback-page =
    .label = ප්‍රතිචාරය යවන්න…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = ඇඩෝන දුබල කර යළි-අරඹන්න…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = සක්‍රීය කළ ඇඩෝන සමඟ යළි අරඹන්න
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = අවිශ්වාසී අඩවිය වාර්ථා කරන්න ...
    .accesskey = D
appmenu-help-not-deceptive =
    .label = මෙය කූට අඩවියක් නොවේ
    .accesskey = d

## More Tools

