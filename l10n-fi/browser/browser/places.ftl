# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.

places-open =
    .label = Avaa
    .accesskey = A
places-open-in-tab =
    .label = Avaa uuteen välilehteen
    .accesskey = u
places-open-tab =
    .label = Avaa uuteen välilehteen
    .accesskey = u
places-open-all-in-tabs =
    .label = Avaa välilehtiin
    .accesskey = A
places-open-window =
    .label = Avaa uuteen ikkunaan
    .accesskey = v
places-open-in-window =
    .label = Avaa uuteen ikkunaan
    .accesskey = v
places-open-private-window =
    .label = Avaa uuteen yksityiseen ikkunaan
    .accesskey = y
places-open-in-private-window =
    .label = Avaa uuteen yksityiseen ikkunaan
    .accesskey = y
places-new-bookmark =
    .label = Uusi kirjanmerkki…
    .accesskey = U
places-new-folder-contextmenu =
    .label = Uusi kansio…
    .accesskey = s
places-new-folder =
    .label = Uusi kansio…
    .accesskey = s
places-new-separator =
    .label = Uusi erotin
    .accesskey = e
places-add-bookmark =
    .label = Lisää kirjanmerkki…
    .accesskey = r
places-add-folder-contextmenu =
    .label = Lisää kansio…
    .accesskey = ä
places-add-folder =
    .label = Lisää kansio…
    .accesskey = o
places-add-separator =
    .label = Lisää erotin
    .accesskey = i
places-view =
    .label = Näytä
    .accesskey = N
places-by-date =
    .label = Päivämäärän mukaan
    .accesskey = P
places-by-site =
    .label = Sivuston mukaan
    .accesskey = S
places-by-most-visited =
    .label = Vierailukertojen mukaan
    .accesskey = k
places-by-last-visited =
    .label = Vierailuajan mukaan
    .accesskey = a
places-by-day-and-site =
    .label = Päivämäärän ja sivuston mukaan
    .accesskey = j
places-history-search =
    .placeholder = Etsi historiasta
places-bookmarks-search =
    .placeholder = Etsi kirjanmerkeistä
places-delete-domain-data =
    .label = Unohda tämä sivusto
    .accesskey = U
places-sortby-name =
    .label = Lajittele nimen mukaan
    .accesskey = L
places-properties =
    .label = Ominaisuudet
    .accesskey = O
# places-edit-bookmark and places-edit-generic will show one or the other and can have the same access key.
places-edit-bookmark =
    .label = Muokkaa kirjanmerkkiä…
    .accesskey = j
places-edit-generic =
    .label = Muokkaa…
    .accesskey = M
# Managed bookmarks are created by an administrator and cannot be changed by the user.
managed-bookmarks =
    .label = Hallinnoidut kirjanmerkit
# This label is used when a managed bookmarks folder doesn't have a name.
managed-bookmarks-subfolder =
    .label = Alikansio
# This label is used for the "Other Bookmarks" folder that appears in the bookmarks toolbar.
other-bookmarks-folder =
    .label = Muut kirjanmerkit
# Variables:
# $count (number) - The number of elements being selected for removal.
places-remove-bookmark =
    .label =
        { $count ->
            [1] Poista kirjanmerkki
           *[other] Poista kirjanmerkit
        }
    .accesskey = o
places-manage-bookmarks =
    .label = Hallinnoi kirjanmerkkejä
    .accesskey = H
