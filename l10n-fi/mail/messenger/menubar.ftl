# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Tekstin merkistö
    .accesskey = T

## Mail Toolbar

toolbar-junk-button =
    .label = Roskapostia
    .tooltiptext = Merkitse valitut viestit roskapostiksi
toolbar-not-junk-button =
    .label = Ei roskapostia
    .tooltiptext = Poista roskapostimerkintä valituilta viesteiltä
toolbar-delete-button =
    .label = Poista
    .tooltiptext = Poista valitut viestit tai kansio
toolbar-undelete-button =
    .label = Peruuta poistaminen
    .tooltiptext = Peruuta valittujen viestien poistaminen
