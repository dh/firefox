# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

troubleshoot-mode-disable-addons =
    .label = Poista käytöstä kaikki lisäosat
    .accesskey = P
troubleshoot-mode-change-and-restart =
    .label = Toteuta muutokset ja käynnistä uudelleen
    .accesskey = T
troubleshoot-mode-quit =
    .label =
        { PLATFORM() ->
            [windows] Lopeta
           *[other] Lopeta
        }
    .accesskey =
        { PLATFORM() ->
            [windows] Q
           *[other] Q
        }
