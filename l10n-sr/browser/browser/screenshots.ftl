# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

screenshots-context-menu = Направи снимак екрана
screenshot-toolbarbutton =
    .label = Снимак екрана
    .tooltiptext = Направи снимак екрана
screenshots-my-shots-button = Моји снимци
screenshots-instructions = Изаберите област превлачењем мишем или кликом на страницу. Притисните Esc да бисте отказали.
screenshots-cancel-button = Откажи
screenshots-save-visible-button = Сачувај видљиво
screenshots-save-page-button = Сачувај целу страницу
screenshots-download-button = Преузми
screenshots-download-button-tooltip = Преузми снимак екрана
screenshots-copy-button = Копирај
screenshots-copy-button-tooltip = Копирај снимак екрана у привремену меморију
screenshots-meta-key =
    { PLATFORM() ->
        [macos] ⌘
       *[other] Ctrl
    }
screenshots-notification-link-copied-title = Линк је копиран
screenshots-notification-link-copied-details = Линк до вашег снимка екрана је копиран у привремену меморију. Налепите га помоћу { screenshots-meta-key }-V.
screenshots-notification-image-copied-title = Снимак је копиран
screenshots-notification-image-copied-details = Ваш снимак је копиран. Притисните { screenshots-meta-key }-V да налепите.
screenshots-request-error-title = Не ради.
screenshots-request-error-details = Нажалост, није могуће сачувати снимак екрана. Покушајте касније.
screenshots-connection-error-title = Повезивање са вашим снимцима екрана није могуће.
screenshots-connection-error-details = Проверите да ли сте повезани на интернет. Уколико јесте, онда можда постоји привремени проблем са услугом { -screenshots-brand-name }.
screenshots-login-error-details = Није могуће сачувати снимак екрана због проблема са услугом { -screenshots-brand-name }. Покушајте касније.
screenshots-unshootable-page-error-title = Не можемо забележити снимак ове странице.
screenshots-unshootable-page-error-details = Ово није стандардна веб страница, тако да не можете забележити њен снимак.
screenshots-self-screenshot-error-title = Не можете да направите снимак екрана странице { -screenshots-brand-name(case: "gen") }.
screenshots-empty-selection-error-title = Ваша селекција је премала
screenshots-private-window-error-title = Услуга { -screenshots-brand-name } је онемогућена у режиму приватног прегледања
screenshots-private-window-error-details = Извињавамо се на непријатности. Радимо на додавању ове функције.
screenshots-generic-error-title = Ау! Услуга { -screenshots-brand-name } је престала са радом.
screenshots-generic-error-details = Нисмо сигурни шта се управо догодило. Желите ли покушати поново или да усликате другачију страницу?
