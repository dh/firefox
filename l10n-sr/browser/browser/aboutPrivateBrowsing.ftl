# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

privatebrowsingpage-open-private-window-label = Отвори приватан прозор
    .accesskey = P
about-private-browsing-search-placeholder = Претражи веб
about-private-browsing-info-title = У приватном прозору сте
about-private-browsing-info-myths = Чести митови о приватном прегледању
about-private-browsing =
    .title = Претражи веб
# Variables
#  $engine (String): the name of the user's default search engine
about-private-browsing-handoff =
    .title = Претражите у претраживачу { $engine } или унесите адресу
about-private-browsing-handoff-no-engine =
    .title = Претражите или унесите адресу
# Variables
#  $engine (String): the name of the user's default search engine
about-private-browsing-handoff-text = Претражите у претраживачу { $engine } или унесите адресу
about-private-browsing-handoff-text-no-engine = Претражите или унесите адресу
about-private-browsing-not-private = Тренутно нисте у приватном прозору.
about-private-browsing-info-description = { -brand-short-name } чисти вашу историју претраживања и прегледања када затворите програм или када затворите све приватне језичке и прозоре. Иако вас ово не чини анонимним вашем интернет провајдеру или на веб сајтовима, ово вам олакшава да сакријете ваше радње на вебу од других корисника овог рачунара.
about-private-browsing-need-more-privacy = Треба вам више приватности?
about-private-browsing-turn-on-vpn = Испробајте { -mozilla-vpn-brand-name }
# This string is the title for the banner for search engine selection
# in a private window.
# Variables:
#   $engineName (String) - The engine name that will currently be used for the private window.
about-private-browsing-search-banner-title = { $engineName } је ваш подразумевани претраживач у приватном претраживању
about-private-browsing-search-banner-description = У <a data-l10n-name="link-options">подешавањима</a> можете да изаберете други претраживач.
about-private-browsing-search-banner-close-button =
    .aria-label = Затвори
