# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Media: AVIF
experimental-features-http3 =
    .label = Протокол HTTP/3
experimental-features-http3-description = Експериментална подршка протокола HTTP/3.
# Search during IME
experimental-features-ime-search =
    .label = Трака за адресу: приказуј резултате током IME уноса
