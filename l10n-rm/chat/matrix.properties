# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (options.*):
#   These are the protocol specific options shown in the account manager and
#   account wizard windows.
options.connectServer=Server
options.connectPort=Port

options.saveToken=Memorisar il token d'access

# LOCALIZATION NOTE (authPrompt):
#   This is the prompt in the browser window that pops up to authorize us
#   to use a Matrix account. It is shown in the title bar of the authorization
#   window.
authPrompt=Permetter dad utilisar tes conto da Matrix

# LOCALIZATION NOTE (connection.*):
#   These will be displayed in the account manager in order to show the progress
#   of the connection.
#   (These will be displayed in account.connection.progress from
#    accounts.properties, which adds … at the end, so do not include
#    periods at the end of these messages.)
connection.requestAuth=Spetgar tia autorisaziun
connection.requestAccess=Terminar l'autentificaziun

# LOCALIZATION NOTE (connection.error.*):
#   These will show in the account manager if an error occurs during the
#   connection attempt.
connection.error.noSupportedFlow=Il server na porscha nagin process d'annunzia cumpatibel.
connection.error.authCancelled=Ti has interrut il process d'autorisaziun.
connection.error.sessionEnded=La sesida è vegnida deconnectada.

# LOCALIZATION NOTE (chatRoomField.*):
#   These are the name of fields displayed in the 'Join Chat' dialog
#   for Matrix accounts.
#   The _ character won't be displayed; it indicates the next
#   character of the string should be used as the access key for this
#   field.
chatRoomField.room=_Local

# LOCALIZATION NOTE (tooltip.*):
#    These are the descriptions given in a tooltip with information received
#    from the "User" object.
# The human readable name of the user.
tooltip.displayName=Num per visualisar
# %S is the timespan elapsed since the last activity.
tooltip.timespan=avant %S
tooltip.lastActive=Ultima activitad

# LOCALIZATION NOTE (powerLevel.*):
#    These are the string representations of different standard power levels and strings.
#    %S are one of the power levels, Default/Moderator/Admin/Restricted/Custom.
powerLevel.default=Standard
powerLevel.moderator=Moderatur
powerLevel.admin=Administratur
powerLevel.restricted=Restrenschì
powerLevel.custom=Persunalisà
#    %1$S is the power level name
#    %2$S is the power level number
powerLevel.detailed=%1$S (%2$S)
powerLevel.defaultRole=Rolla da standard: %S
powerLevel.inviteUser=Envidar utilisaders: %S
powerLevel.kickUsers=Sclauder utilisaders: %S
powerLevel.ban=Bandischar utilisaders: %S
powerLevel.roomAvatar=Midar l'avatar da la stanza: %S
powerLevel.mainAddress=Midar l'adressa principala da la stanza: %S
powerLevel.history=Midar la visibilitad da la cronologia: %S
powerLevel.roomName=Midar il num da la stanza: %S
powerLevel.changePermissions=Midar las permissiuns: %S
powerLevel.server_acl=Trametter eveniments m.room.server_acl: %S
powerLevel.upgradeRoom=Upgrade da la stanza: %S
powerLevel.remove=Allontanar ils messadis: %S
powerLevel.events_default=Standard dad eveniments: %S
powerLevel.state_default=Midar il parameter: %S
powerLevel.encryption=Activar il criptadi per la stanza: %S
powerLevel.topic=Definir il tema da la stanza: %S

# LOCALIZATION NOTE (detail.*):
#    These are the string representations of different matrix properties.
#    %S will typically be strings with the actual values.
# Example placeholder: "Foo bar"
detail.name=Num: %S
# Example placeholder: "My first room"
detail.topic=Tema: %S
# Example placeholder: "5"
detail.version=Versiun da la stanza: %S
# Example placeholder: "#thunderbird:mozilla.org"
detail.roomId=RoomID: %S
# %S are all admin users. Example: "@foo:example.com, @bar:example.com"
detail.admin=Admin: %S
# %S are all moderators. Example: "@lorem:mozilla.org, @ipsum:mozilla.org"
detail.moderator=ModeraturA: %S
# Example placeholder: "#thunderbird:matrix.org"
detail.alias=Alias: %S
# Example placeholder: "can_join"
detail.guest=Access dad osp: %S
# This is a heading, followed by the powerLevel.* strings
detail.power=Nivels:

# LOCALIZATION NOTE (command.*):
#   These are the help messages for each command, the %S is the command name
#   Each command first gives the parameter it accepts and then a description of
#   the command.
command.ban=%S &lt;userId&gt; [&lt;reason&gt;]: Bandischar l'utilisadra u l'utilisader cun la userId da la stanza cun ina motivaziun facultativa. La permissiun da bandischar utilisaders è necessaria.
command.invite=%S &lt;userId&gt;: Envidar l'utilisadra u l'utilisader en la stanza.
command.kick=%S &lt;userId&gt; [&lt;reason&gt;]: Sclauder l'utilisader cun la userId da la stanza cun in messadi da motivaziun facultativ. La permissiun da sclauder utilisaders è necessaria.
command.nick=%S &lt;display_name&gt;: Midar tes num per mussar.
command.op=%S &lt;userId&gt; [&lt;power level&gt;]: Definescha il nivel da pussanza da mintga utilisader. Endatescha ina valur integer (cifra), Utilisader: 0, Moderatur: 50 ed Admin: 100. La valur predefinida è 50, sch'i na vegn inditgà nagins arguments. La permissiun da midar il nivel da pussanza dad in commember è necessaria. Na funcziuna betg per auters admins, mo per tatez.
command.deop=%S &lt;userId&gt;: Redefinir il nivel da pussanza da l'utilisader cun 0 (Utilisader). Il dretg da midar ils nivels da pussanza dals commembers è necessari. Na funcziuna betg per auters admins, mo per tatez.
command.leave=%S: Bandunar la stanza actuala.
command.topic=%S &lt;tema&gt;: Definescha il tema da la stanza. La permissiun da midar il tema da la stanza è necessaria.
command.unban=%S &lt;userId&gt;: Annullescha la bandischun dad in utilisader ch'è bandischà da la stanza. La permissiun da bandischar utilisaders è necessaria.
command.visibility=%S [&lt;visibilitad&gt;]: Definescha la visibilitad da la stanza actuala en il register da stanzas dal home server actual. Endatar ina valur integer (dumber entir), Privat: 0 e Public: 1. Predefinì è Privat (0) sch'i na vegn inditgà nagin argument. La permissiun da midar la visibilitad da stanzas è necessaria.
command.guest=%S &lt;access dad osps&gt; &lt;visibilitad da la cronologia&gt;: Definescha l'access e la visibilitad da la cronologia da la stanza actuala per ils utilisaders osps. Endatescha duas valurs integer (dumbers entirs): L'emprim per l'access dad osp (betg permess: 0 e permess: 1) e la segunda per la cronologia (betg visibel: 0 e visibel: 1). Pretenda la permissiun da midar la visibilitad da la cronologia.
command.roomname=%S &lt;num&gt;: Definescha il num da la stanza. La permissiun da midar il num da la stanza è necessaria.
command.detail=%S: Mussar ils detagls da la stanza.
command.addalias=%S &lt;alias&gt;: Creescha in alias per la stanza. La furma dal num spetgada è '#numstanza:domena'. La permissiun dad agiuntar alias è necessaria.
command.removealias=%S &lt;alias&gt;: Allontanar l'alias per la stanza. La furma spetgada da l'alias da la stanza è '#numstanza:domena'. La permissiun dad allontanar alias è necessaria.
command.upgraderoom=%S &lt;novaVersiun&gt;: Upgrade da la stanza a la versiun inditgada. La permissiun dad upgrades da stanzas è necessaria.
command.me=%S &lt;acziun&gt;: Exequir ina acziun.
command.msg=%S &lt;userId&gt; &lt;messadi&gt;: Trametter in messadi direct ad in utilisader specifitgà.
command.join=%S &lt;roomId&gt;: Entrar en la stanza inditgada.

# LOCALIZATION NOTE (message.*):
#    These are shown as system messages in the conversation.
#    %S is the reason string for the particular action.
#    Used within context of ban, kick and withdrew invite.
message.reason=Motiv: %S.
#    Gets message.reason appended, if a reason was specified.
#    %1$S is the name of the user who banned.
#    %2$S is the name of the user who got banned.
#    %1$S is the name of the user who accepted the invitation.
#    %2$S is the name of the user who sent the invitation.
#    %S is the name of the user who accepted an invitation.
#    %1$S is the name of the user who invited.
#    %2$S is the name of the user who got invited.
#    %1$S is the name of the user who changed their display name.
#    %2$S is the old display name.
#    %3$S is the new display name.
#    %1$S is the name of the user who set their display name.
#    %2$S is the newly set display name.
#    %1$S is the name of the user who removed their display name.
#    %2$S is the old display name which has been removed.
#    %S is the name of the user who has joined the room.
#    %S is the name of the user who has rejected the invitation.
#    %S is the name of the user who has left the room.
#    %1$S is the name of the user who unbanned.
#    %2$S is the name of the user who got unbanned.
#    Gets message.reason appended, if a reason was specified.
#    %1$S is the name of the user who kicked.
#    %2$S is the name of the user who got kicked.
#    Gets message.reason appended, if a reason was specified.
#    %1$S is the name of the user who withdrew invitation.
#    %2$S is the name of the user whose invitation has been withdrawn.
#    %S is the name of the user who has removed the room name.
#    %1$S is the name of the user who changed the room name.
#    %2$S is the new room name.
#    %1$S is the name of the user who changed the power level.
#    %2$S is a list of "message.powerLevel.fromTo" strings representing power level changes separated by commas
#    power level changes, separated by commas if  there are multiple changes.
#    %1$S is the name of the target user whose power level has been changed.
#    %2$S is the old power level.
#    %2$S is the new power level.
#    %S is the name of the user who has allowed guests to join the room.
#    %S is the name of the user who has prevented guests to join the room.
#    %S is the name of the user who has made future room history visible to anyone.
#    %S is the name of the user who has made future room history visible to all room members.
#    %S is the name of the user who has made future room history visible to all room members, from the point they are invited.
#    %S is the name of the user who has made future room history visible to all room members, from the point they joined.
#    %1$S is the name of the user who changed the address.
#    %2$S is the old address.
#    %3$S is the new address.
#    %1$S is the name of the user who added the address.
#    %2$S is a comma delimited list of added addresses.
#    %1$S is the name of the user who removed the address.
#    %2$S is a comma delimited list of removed addresses.
#    %1$S is the name of the user that edited the alias addresses.
#    %2$S is a comma delimited list of removed addresses.
#    %3$S is a comma delmited list of added addresses.
