# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-rights-notification-text = { -brand-short-name } è in program gratuit ed open-source, realisà dad ina communitad da millis persunas da tut il mund.

## Folder Pane

folder-pane-toolbar =
    .toolbarname = Trav d'utensils da la panela d'ordinaturs
    .accesskey = T
folder-pane-toolbar-options-button =
    .tooltiptext = Opziuns da la zona d'agiuntas
folder-pane-header-label = Ordinaturs

## Folder Toolbar Header Popup

folder-toolbar-hide-toolbar-toolbarbutton =
    .label = Zuppentar la trav d'utensils
    .accesskey = Z
show-all-folders-label =
    .label = Tut ils ordinaturs
    .accesskey = a
show-unread-folders-label =
    .label = Ordinaturs nunlegids
    .accesskey = n
show-favorite-folders-label =
    .label = Ordinaturs preferids
    .accesskey = f
show-smart-folders-label =
    .label = Ordinaturs gruppads
    .accesskey = u
show-recent-folders-label =
    .label = Ultims ordinaturs
    .accesskey = r
folder-toolbar-toggle-folder-compact-view =
    .label = Vista cumpacta
    .accesskey = c
