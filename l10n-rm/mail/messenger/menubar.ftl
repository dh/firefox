# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Codaziun dal text
    .accesskey = C

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Modus per schliar problems…
    .accesskey = M
menu-help-exit-troubleshoot-mode =
    .label = Deactivar il modus per schliar problems
    .accesskey = o
menu-help-more-troubleshooting-info =
    .label = Dapli infurmaziuns per schliar problems
    .accesskey = m

## Mail Toolbar

toolbar-junk-button =
    .label = Nungiavischà
    .tooltiptext = Marcar ils messadis tschernids sco nungiavischads
toolbar-not-junk-button =
    .label = Betg nungiavischà
    .tooltiptext = Betg marcar ils messadis tschernids sco nungiavischads
toolbar-delete-button =
    .label = Stizzar
    .tooltiptext = Stizzar ils messadis tschernids u l'ordinatur
toolbar-undelete-button =
    .label = Revocar il stizzar
    .tooltiptext = Revocar l'eliminaziun dals messadis tschernids
