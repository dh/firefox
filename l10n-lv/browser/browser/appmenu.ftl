# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Pielāgot…

## Zoom Controls

appmenuitem-new-window =
    .label = Jauns logs
appmenuitem-new-private-window =
    .label = Jauns privātais logs

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = Pa visu ekrānu

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sinhronizēt
appmenuitem-save-page =
    .label = Saglabāt lapu kā…

## What's New panel in App menu.

whatsnew-panel-header = Kas jauns?

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Par { -brand-shorter-name }
    .accesskey = a
appmenu-help-troubleshooting-info =
    .label = Problēmu novēršanas informācija
    .accesskey = P
appmenu-help-taskmanager =
    .label = Uzdevumu pārvaldnieks
appmenu-help-report-site-issue =
    .label = Ziņot par problēmu ar lapu…
appmenu-help-feedback-page =
    .label = Nosūtīt atsauksmi…
    .accesskey = s

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Pārstartēt ar deaktivētiem papildinājumiem…
    .accesskey = r
appmenu-help-safe-mode-with-addons =
    .label = Pārstartēt ar aktivētiem papildinājumiem
    .accesskey = r

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Ziņot par maldinošu lapu…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = Šī nav maldinoša lapa…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = Uzdevumu pārvaldnieks
