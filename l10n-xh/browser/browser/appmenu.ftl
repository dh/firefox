# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Lungiselela…

## Zoom Controls

appmenuitem-new-window =
    .label = Ifestile entsha
appmenuitem-new-private-window =
    .label = Ifestile yangasese entsha

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = Isikrini esiZeleyo

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Ngqamanisa ngoku
appmenuitem-save-page =
    .label = Gcina iphepha njenge…

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Malunga ne-{ -brand-shorter-name }
    .accesskey = M
appmenu-help-troubleshooting-info =
    .label = Inkcazelo yesisombululi-ngxaki
    .accesskey = I
appmenu-help-feedback-page =
    .label = Thumela ingxelo…
    .accesskey = T

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Qalisa kwakhona izongezelelo ziqhwalelisiwe…
    .accesskey = Q
appmenu-help-safe-mode-with-addons =
    .label = Qalisa kwakhona izongezelelo ziqhwalelisiwe…
    .accesskey = Q

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Xela isayithi yenkohliso…
    .accesskey = y
appmenu-help-not-deceptive =
    .label = Le asiyosayithi yenkohliso…
    .accesskey = d

## More Tools

