# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = { -brand-shorter-name } yeniləməsi endirilir
appmenuitem-customize-mode =
    .label = Fərdiləşdir…

## Zoom Controls

appmenuitem-new-window =
    .label = Yeni Pəncərə
appmenuitem-new-private-window =
    .label = Yeni Məxfi Pəncərə

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Yaxınlaşdır
appmenuitem-zoom-reduce =
    .label = Uzaqlaşdır
appmenuitem-fullscreen =
    .label = Tam ekran

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = İndi Sinxronizə et
appmenuitem-save-page =
    .label = Fərqli saxla…

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = { -brand-shorter-name } Haqqında
    .accesskey = H
appmenu-help-troubleshooting-info =
    .label = Problemlərin aradan qaldırılması üzrə məlumatlar
    .accesskey = P
appmenu-help-taskmanager =
    .label = Tapşırıq idarə edicisi
appmenu-help-report-site-issue =
    .label = Sayt Problemini Bildir…
appmenu-help-feedback-page =
    .label = Əks əlaqə göndər…
    .accesskey = g

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Əlavələri deaktiv edərək Yenidən başla…
    .accesskey = Y
appmenu-help-safe-mode-with-addons =
    .label = Əlavələri aktiv edərək Yenidən başla
    .accesskey = Y

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Aldadıcı sayt xəbər et…
    .accesskey = D
appmenu-help-not-deceptive =
    .label = Bu aldadıcı sayt deyil…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = Tapşırıq idarə edicisi
