# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Tegnkodning
    .accesskey = T

## Mail Toolbar

toolbar-junk-button =
    .label = Spam
    .tooltiptext = Marker den valgte meddelelse som spam
toolbar-not-junk-button =
    .label = Ikke-spam
    .tooltiptext = Marker den valgte meddelelse som ikke-spam
toolbar-delete-button =
    .label = Slet
    .tooltiptext = Slet den valgte meddelelse eller mappe
toolbar-undelete-button =
    .label = Fortryd sletning
    .tooltiptext = Fortryd sletning af valgte meddelelser
