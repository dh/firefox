# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Testuaren kodeketa
    .accesskey = k

## Mail Toolbar

toolbar-junk-button =
    .label = Zaborra
    .tooltiptext = Markatu aukeratutako mezua zabor bezala
toolbar-not-junk-button =
    .label = Ez da zaborra
    .tooltiptext = Markatu aukeratutako mezua EZ zabor bezala
toolbar-delete-button =
    .label = Ezabatu
    .tooltiptext = Ezabatu aukeratutako mezu edo karpeta
toolbar-undelete-button =
    .label = Desezabatu
    .tooltiptext = Desezabatu aukeratutako mezuak
