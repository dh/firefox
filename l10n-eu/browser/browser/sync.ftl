# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-toolbar-sync-syncing =
    .label = Sinkronizatzen…
fxa-toolbar-sync-syncing-tabs =
    .label = Fitxak sinkronizatzen…
fxa-toolbar-sync-syncing2 = Sinkronizatzen…
sync-disconnect-dialog-title = Deskonektatu { -sync-brand-short-name }?
sync-disconnect-dialog-body = { -brand-product-name }(e)k zure kontuarekin sinkronizatzeari utziko dio baina ez du gailu honetako zure nabigatze-daturik ezabatuko.
fxa-disconnect-dialog-title = Deskonektatu { -brand-product-name }?
fxa-disconnect-dialog-body = { -brand-product-name } zure kontutik deskonektatuko da baina ez du gailu honetako zure nabigatze-daturik ezabatuko.
sync-disconnect-dialog-button = Deskonektatu
fxa-signout-dialog-heading = Itxi { -fxaccount-brand-name } saioa?
fxa-signout-dialog-body = Sinkronizatutako datuek zure kontuan jarraituko dute.
fxa-signout-checkbox =
    .label = Ezabatu gailu honetako datuak (saio-hasierak, pasahitzak, historia, laster-markak, etab.).
fxa-signout-dialog =
    .title = Itxi { -fxaccount-brand-name } saioa
    .style = min-width: 375px;
    .buttonlabelaccept = Itxi saioa
fxa-menu-connect-another-device =
    .label = Konektatu beste gailu bat…
fxa-menu-sign-out =
    .label = Amaitu saioa…
