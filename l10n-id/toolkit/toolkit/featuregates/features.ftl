# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-masonry2 =
    .label = CSS: Masonry Layout
experimental-features-css-masonry-description = Mengaktifkan dukungan untuk fitur Layout CSS Masonry eksperimental. Lihat <a data-l10n-name="explainer">penjelasan</a> untuk mendapatkan deskripsi fitur yang lebih lengkap. Untuk memberikan umpan balik, silakan berkomentar di <a data-l10n-name="w3c-issue">isu GitHub ini</a> atau <a data-l10n-name="bug">bug ini</a>.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-gpu2 =
    .label = API Web: WebGPU
experimental-features-web-gpu-description2 = API baru ini menyediakan dukungan tingkat-rendah untuk melakukan komputasi dan perenderan grafis dengan menggunakan <a data-l10n-name="wikipedia">Unit Pemrosesan Grafik (GPU)</a> dari perangkat atau komputer pengguna. <a data-l10n-name="spec">Spesifikasi</a> masih dalam proses pengembangan. Lihat <a data-l10n-name="bugzilla">bug 1602129</a> untuk detail lebih lanjut.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-media-avif =
    .label = Media: AVIF
experimental-features-media-avif-description = Jika fitur ini diaktifkan, { -brand-short-name } akan mendukung format Berkas Gambar AV1 (AVIF). Ini adalah format berkas gambar diam yang memanfaatkan kemampuan algoritme kompresi video AV1 untuk mengurangi ukuran gambar. Lihat <a data-l10n-name="bugzilla">bug 1443863</a> untuk detail lebih lanjut.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-web-api-inputmode =
    .label = API Web: inputmode
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-css-constructable-stylesheets =
    .label = CSS: Constructable Stylesheet
experimental-features-devtools-color-scheme-simulation =
    .label = Alat Pengembang: Simulasi Skema Warna
experimental-features-devtools-execution-context-selector =
    .label = Alat Pengembang: Pemilih Konteks Eksekusi
experimental-features-devtools-compatibility-panel =
    .label = Alat Pengembang: Panel Kompabilitas
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Kuki: SameSite=Lax secara baku
experimental-features-cookie-samesite-lax-by-default2-description = Perlakukan kuki sebagai “SameSite=Lax” secara baku jika tidak ada atribut “SameSite” ditentukan. Pengembang harus memilih status quo saat ini dari penggunaan yang tidak terbatas dengan secara eksplisit menyatakan “SameSite=None””.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-none-requires-secure2 =
    .label = Kuki: SameSite=None memerlukan atribut aman
experimental-features-cookie-samesite-none-requires-secure2-description = Kuki dengan atribut “SameSite=None” memerlukan atribut aman. Fitur ini memerlukan "Kuki: SameSite=Lax secara baku".
# about:home should be kept in English, as it refers to the the URI for
# the internal default home page.
experimental-features-abouthome-startup-cache =
    .label = Tembolok awal about:home
experimental-features-abouthome-startup-cache-description = Tembolok untuk dokumen about:home awal yang dimuat secara baku pada saat memulai. Tujuan dari tembolok ini adalah untuk meningkatkan kinerja proses mulai.
# The title of the experiment should be kept in English as it may be referenced
# by various online articles and is technical in nature.
experimental-features-cookie-samesite-schemeful =
    .label = Kuki: SameSite Berskema
# "Service Worker" is an API name and is usually not translated.
experimental-features-devtools-serviceworker-debugger-support =
    .label = Alat Pengembang: Debugging Service Worker
# JS JIT Warp project
experimental-features-js-warp =
    .label = JavaScript JIT: Warp
experimental-features-js-warp-description = Aktifkan Warp, sebuah proyek untuk meningkatkan kinerja JavaScript dan penggunaan memori.
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (Isolasi Situs)
# Support for having multiple Picture-in-Picture windows open simultaneously
experimental-features-multi-pip =
    .label = Dukungan Beberapa Gambar-dalam-Gambar
experimental-features-multi-pip-description = Dukungan eksperimental untuk memungkinkan beberapa jendela Gambar-dalam-Gambar dibuka pada saat yang sama.
experimental-features-http3 =
    .label = Protokol HTTP/3
experimental-features-http3-description = Dukungan eksperimental untuk protokol HTTP/3.
# Search during IME
experimental-features-ime-search =
    .label = Bilah Alamat: Tampilkan hasil selama komposisi IME
