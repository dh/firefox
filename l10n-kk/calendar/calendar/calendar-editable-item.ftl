# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-editable-item-privacy-icon-private =
    .alt = Жекелік: Жеке оқиға
calendar-editable-item-privacy-icon-confidential =
    .alt = Жекелік: Тек уақыт пен күнді көрсету
