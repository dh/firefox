# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Variables:
# $count (Number) - Number of unread messages.
unread-messages-os-tooltip =
    { $count ->
        [one] 1 mesazh i palexuar
       *[other] { $count } mesazhe të palexuar
    }
about-rights-notification-text = { -brand-short-name } është program i lirë dhe me burim të hapët, i krijuar nga një bashkësi mijëra vetash nga anembanë bota.

## Folder Pane

folder-pane-toolbar =
    .toolbarname = Panel Kuadrati Dosjesh
    .accesskey = P
folder-pane-toolbar-options-button =
    .tooltiptext = Mundësi Kuadrati Dosjesh
folder-pane-header-label = Dosje

## Folder Toolbar Header Popup

folder-toolbar-hide-toolbar-toolbarbutton =
    .label = Fshihe Panelin
    .accesskey = F
show-all-folders-label =
    .label = Tërë Dosjet
    .accesskey = T
show-unread-folders-label =
    .label = Dosje për Të palexuarit
    .accesskey = a
show-favorite-folders-label =
    .label = Dosje për Të parapëlqyerit
    .accesskey = q
show-smart-folders-label =
    .label = Dosje të Njësuara
    .accesskey = j
show-recent-folders-label =
    .label = Dosje për Të fundit
    .accesskey = f
folder-toolbar-toggle-folder-compact-view =
    .label = Parje e Ngjeshur
    .accesskey = N
