# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Kodim Teksti
    .accesskey = K

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Mënyra Diagnostikim…
    .accesskey = D
menu-help-exit-troubleshoot-mode =
    .label = Çaktivizo Mënyrën Diagnostikim
    .accesskey = Ç
menu-help-more-troubleshooting-info =
    .label = Më Tepër të Dhëna Diagnostikimi
    .accesskey = M

## Mail Toolbar

toolbar-junk-button =
    .label = I pavlerë
    .tooltiptext = Vëri shenjë si i pavlerë mesazhit të përzgjedhur
toolbar-not-junk-button =
    .label = Jo i Pavlerë
    .tooltiptext = Vëri shenjë si jo i pavlerë mesazhit të përzgjedhur
toolbar-delete-button =
    .label = Fshije
    .tooltiptext = Fsji mesazhet ose dosjen e përzgjedhur
toolbar-undelete-button =
    .label = Çfshije
    .tooltiptext = Hiqe fshirjen e mesazheve të përzgjedhur
