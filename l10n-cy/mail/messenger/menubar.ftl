# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Amgodiad Testun
    .accesskey = A

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Y Modd Datrys Problemau…
    .accesskey = M
menu-help-exit-troubleshoot-mode =
    .label = Diffodd y Modd Dartrys Problemau
    .accesskey = D
menu-help-more-troubleshooting-info =
    .label = Rhagor o Wybodaeth Datrys Problemau
    .accesskey = R

## Mail Toolbar

toolbar-junk-button =
    .label = Sbwriel
    .tooltiptext = Marcio'r negeseuon hyn fel sbwriel
toolbar-not-junk-button =
    .label = Nid Sbwriel
    .tooltiptext = Marcio'r negeseuon hyn fel nid sbwriel
toolbar-delete-button =
    .label = Dileu
    .tooltiptext = Dileu'r negeseuon neu ffolderi hyn
toolbar-undelete-button =
    .label = Dad-ddileu
    .tooltiptext = Dad-ddileu'r negeseuon hyn
