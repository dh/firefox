# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
---
job-defaults:
    max-run-time:
        by-test-platform:
            .*-qr/.*: 2400
            .*-ref-hw-2017/.*: 3600
            default: 1800
    suite: raptor
    run-on-projects:
        by-test-platform:
            linux.*shippable[^-qr].*: ['mozilla-central']
            default: []
    test-manifest-loader: null  # don't load tests in the taskgraph
    tier: 3
    fission-tier: 3
    virtualization: hardware
    mozharness:
        script: raptor_script.py
        config:
            by-test-platform:
                macosx.*:
                    - raptor/mac_config.py
                windows.*:
                    - raptor/windows_config.py
                default:
                    - raptor/linux_config.py
        extra-options:
            - --browsertime
            - --no-conditioned-profile
    fission-run-on-projects: []
    variants:
        by-app:
            firefox:
                by-test-platform:
                    .*shippable-qr/.*: [fission]
                    default: []
            default: []
    python-3: true

browsertime-tp6:
    description: "Raptor (browsertime) tp6 page-load tests"
    raptor-test: tp6
    raptor-subtests:
        - amazon
        - apple
        - [bing-search, bing]
        - ebay
        - [facebook, fb]
        - [facebook-redesign, fbr]
        - fandom
        - [google-docs, gdocs]
        - [google-mail, gmail]
        - [google-search, gsearch]
        - [google-sheets, gsheets]
        - [google-slides, gslides]
        - imdb
        - imgur
        - instagram
        - linkedin
        - microsoft
        - netflix
        - office
        - outlook
        - paypal
        - pinterest
        - reddit
        - tumblr
        - twitch
        - twitter
        - wikipedia
        - yahoo-mail
        - yahoo-news
        - yandex
        - youtube
    tier:
        by-app:
            firefox:
                by-subtest:
                    amazon: 1
                    google-mail: 1
                    google-slides: 1
                    imgur: 1
                    tumblr: 1
                    twitch: 1
                    twitter: 1
                    default: 2
            default: 3
    apps: ['firefox', 'chrome', 'chromium']
    fission-tier: 2
    fission-run-on-projects:
        by-test-platform:
            windows7.*: []
            default: [mozilla-central]
    run-on-projects:
        by-app:
            firefox:
                by-test-platform:
                    (linux|windows|macos)(?!.*shippable).*: []
                    linux.*shippable.*: ["trunk", "mozilla-beta"]
                    macos.*shippable.*: ["trunk", "mozilla-beta"]
                    windows10.*shippable.*: ["trunk", "mozilla-beta"]
                    default: []
            default: []
    limit-platforms:
        by-app:
            chrome:
                - linux.*shippable[^-qr].*
                - macosx1015-64-shippable-qr/opt
                - windows10.*shippable[^-qr].*
            chromium:
                # Run on non-QR shippable, but test-platforms.yml doesn't have
                # any more non-QR macosx shippable things. So for macosx we want
                # to run it on shippable-qr, otherwise there's nothing running
                # this on macosx at all.
                - .*shippable[^-qr].*
                - macosx1015-64-shippable-qr/opt
                - windows10.*shippable[^-qr].*
            default: []
    treeherder-symbol: Btime(tp6)
    max-run-time: 4000
    run-visual-metrics: true
    mozharness:
        extra-options:
            by-test-platform:
                windows10.*shippable.*-qr.*:
                    - --chimera
                    - --browsertime
                    - --no-conditioned-profile
                    - --browsertime-no-ffwindowrecorder
                default:
                    - --chimera
                    - --browsertime
                    - --no-conditioned-profile

browsertime-benchmark:
    description: "Raptor (browsertime) Benchmark tests "
    raptor-subtests:
        - [speedometer, sp]
        - ares6
        - [motionmark-animometer, mm-a]
        - [motionmark-htmlsuite, mm-h]
        - [stylebench, sb]
    tier:
        by-app:
            firefox:
                by-subtest:
                    motionmark-animometer: 1
                    default:
                        by-test-platform:
                            windows10-64-ref-hw-2017/opt: 2
                            windows10-64-ccov.*/.*: 3
                            linux64-ccov.*/.*: 3
                            default: 2
            default: 2
    fission-run-on-projects:
        by-test-platform:
            windows7.*: []
            default: [mozilla-central]
    run-on-projects:
        by-app:
            firefox:
                by-test-platform:
                    (linux|windows|macos)(?!.*shippable).*: []
                    linux.*shippable.*: ["trunk", "mozilla-beta"]
                    macos.*shippable.*: ["trunk", "mozilla-beta"]
                    windows10.*shippable.*: ["trunk", "mozilla-beta"]
                    default: []
            default: []
    apps: ["firefox"]
    treeherder-symbol: Btime()

browsertime-tp6-live:
    description: "Raptor (browsertime) tp6 on live-sites"
    raptor-test: tp6
    raptor-subtests:
        - amazon
        - apple
        - [bing-search, bing]
        - ebay
        - [facebook, fb]
        - [facebook-redesign, fbr]
        - fandom
        - [google-docs, gdocs]
        - [google-mail, gmail]
        - [google-search, gsearch]
        - [google-sheets, gsheets]
        - [google-slides, gslides]
        - imdb
        - imgur
        - instagram
        - linkedin
        - microsoft
        - netflix
        # - office (site loads blank page if not signed in)
        - outlook
        - paypal
        - pinterest
        - reddit
        - tumblr
        - twitch
        - twitter
        - wikipedia
        - yahoo-mail
        - yahoo-news
        - yandex
        - youtube
    apps: [firefox, chrome, chromium]
    tier: 3
    run-on-projects: []
    run-visual-metrics: true
    treeherder-symbol: Btime-live(tp6)
    mozharness:
        extra-options:
            - --browser-cycles=15
            - --chimera
            - --live-sites

browsertime-tp6-live-sheriffed:
    description: "Raptor (browsertime) tp6 on live-sites"
    raptor-test: tp6
    raptor-subtests:
        - [cnn-ampstories, cnn-amp]
    apps: [firefox]
    tier: 2
    run-on-projects:
        by-test-platform:
            (linux|windows|macos)(?!.*shippable).*: []
            windows7.*shippable.*: []
            default: [autoland]
    run-visual-metrics: true
    treeherder-symbol: Btime-live(tp6)
    mozharness:
        extra-options:
            - --chimera
            - --live-sites

browsertime-tp6-profiling:
    description: "Raptor (browsertime) tp6 page-load tests with Gecko Profiling"
    raptor-test: tp6
    raptor-subtests: ['amazon']
    apps: ['firefox']
    treeherder-symbol: Btime-Prof(tp6)
    max-run-time: 4000
    mozharness:
        extra-options:
            - --chimera
            - --gecko-profile
