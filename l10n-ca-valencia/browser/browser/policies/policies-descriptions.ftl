# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Defineix un URL personalitzat d'actualització de l'aplicació.
policy-BlockAboutAddons = Bloca l'accés al gestor de complements (about:addons).
policy-BlockAboutConfig = Bloca l'accés a la pàgina about:config.
policy-BlockAboutProfiles = Bloca l'accés a la pàgina about:profiles.
policy-BlockAboutSupport = Bloca l'accés a la pàgina about:support.
policy-Bookmarks = Crea adreces d'interés a la barra d'adreces d'interés, al menú d'adreces d'interés o a la carpeta especificada.
policy-CaptivePortal = Activa o desactiva la funcionalitat de portal captiu.
policy-Cookies = Permet o denega que els llocs web definisquen galetes.
policy-DisabledCiphers = Desactiva els xifratges.
policy-DefaultDownloadDirectory = Defineix el directori de baixades per defecte.
policy-DisableAppUpdate = Evita que el navegador s’actualitze.
policy-DisableBuiltinPDFViewer = Desactiva el PDF.js, el visor de PDF incorporat en el { -brand-short-name }.
policy-DisableDeveloperTools = Bloca l'accés a les eines per a desenvolupadors.
policy-DisableFirefoxAccounts = Desactiva els serveis basats en el { -fxaccount-brand-name }, que inclou el Sync.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Desactiva la funció de captures de pantalla del Firefox Screenshots.
policy-DisableFirefoxStudies = Impedeix que el { -brand-short-name } execute estudis.
policy-DisableForgetButton = Evita l'accés al botó Oblida.
policy-DisableFormHistory = No recorda l'historial de cerca ni de formularis.
policy-DisableMasterPasswordCreation = Si és cert, no es pot crear una contrasenya mestra.
policy-DisablePrimaryPasswordCreation = Si és cert, no es pot crear una contrasenya principal.
policy-DisablePocket = Desactiva la funció de guardar llocs web al Pocket.
policy-DisablePrivateBrowsing = Desactiva la navegació privada.
policy-DisableTelemetry = Desactiva la telemetria.
policy-DNSOverHTTPS = Configura DNS sobre HTTPS.
policy-DownloadDirectory = Defineix i bloca el directori de baixades.
policy-ExtensionUpdate = Activa o desactiva les actualitzacions automàtiques de les extensions.
policy-FirefoxHome = Configura el Firefox Home.
policy-FlashPlugin = Permet o denega l'ús del connector Flash.
policy-HardwareAcceleration = Si és fals, desactiva l'acceleració de maquinari.
# “lock” means that the user won’t be able to change this setting
policy-Homepage = Defineix i, opcionalment, bloca la pàgina d'inici.
policy-InstallAddonsPermission = Permet que determinats llocs web instal·len complements.

## Do not translate "SameSite", it's the name of a cookie attribute.


##

policy-LocalFileLinks = Permetre que llocs web específics enllacin a fitxers locals.
policy-MasterPassword = Requerir o impedir l'ús d'una contrasenya mestra.
policy-PrimaryPassword = Requerir o impedir l'ús d'una contrasenya principal.
policy-Proxy = Configura els paràmetres del servidor intermediari.
policy-SearchSuggestEnabled = Activa o desactiva els suggeriments de cerca.
# For more information, see https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/PKCS11/Module_Installation
policy-SecurityDevices = Instal·la mòduls PKCS #11.
policy-SSLVersionMax = Defineix la versió màxima de SSL.
policy-SSLVersionMin = Defineix la versió mínima de SSL.
