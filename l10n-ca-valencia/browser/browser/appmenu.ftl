# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-update-banner =
    .label-update-downloading = S'està baixant l'actualització del { -brand-shorter-name }
appmenuitem-protection-dashboard-title = Tauler de proteccions
appmenuitem-customize-mode =
    .label = Personalitza…

## Zoom Controls

appmenuitem-new-window =
    .label = Finestra nova
appmenuitem-new-private-window =
    .label = Finestra privada nova

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Amplia
appmenuitem-zoom-reduce =
    .label = Redueix
appmenuitem-fullscreen =
    .label = Pantalla completa

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sincronitza ara
appmenuitem-save-page =
    .label = Anomena i guarda la pàgina…

## What's New panel in App menu.

whatsnew-panel-header = Novetats
# Checkbox displayed at the bottom of the What's New panel, allowing users to
# enable/disable What's New notifications.
whatsnew-panel-footer-checkbox =
    .label = Informa'm de les característiques noves
    .accesskey = f

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Quant al { -brand-shorter-name }
    .accesskey = Q
appmenu-help-troubleshooting-info =
    .label = Informació de resolució de problemes
    .accesskey = r
appmenu-help-taskmanager =
    .label = Gestor de tasques
appmenu-help-report-site-issue =
    .label = Informa d'un problema amb el lloc…
appmenu-help-feedback-page =
    .label = Envia comentaris…
    .accesskey = E

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Reinicia amb els complements inhabilitats…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = Reinicia amb els complements habilitats
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Informa que el lloc és enganyós…
    .accesskey = I
appmenu-help-not-deceptive =
    .label = No és cap lloc enganyós…
    .accesskey = N

## More Tools

appmenu-taskmanager =
    .label = Gestor de tasques
