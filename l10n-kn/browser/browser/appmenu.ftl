# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = ನನ್ನಿಚ್ಛೆಗೆ ತಕ್ಕಂತೆ ಹೊಂದಿಸು...

## Zoom Controls

appmenuitem-new-window =
    .label = ಹೊಸ ಕಿಟಕಿ
appmenuitem-new-private-window =
    .label = ಹೊಸ ಖಾಸಗಿ ಕಿಟಕಿ

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = ಪೂರ್ಣ ಪರದೆ

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = ಈಗಲೆ ಸಿಂಕ್ ಮಾಡು
appmenuitem-save-page =
    .label = ಈ ಪುಟವನ್ನು ಹೀಗೆ ಉಳಿಸು...

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = About { -brand-shorter-name }
    .accesskey = A
appmenu-help-troubleshooting-info =
    .label = Troubleshooting ಮಾಹಿತಿ
    .accesskey = T
appmenu-help-report-site-issue =
    .label = ಜಾಲದ ತೊಂದರೆ ವರದಿ ಮಾಡಿ…
appmenu-help-feedback-page =
    .label = Submit ಅಭಿಪ್ರಾಯ…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Restart ಆಡ್‌-ಆನ್‌ಗಳನ್ನು ಅಶಕ್ತಗೊಳಿಸಿದ ನಂತರ…
    .accesskey = R
appmenu-help-safe-mode-with-addons =
    .label = Restart ಆಡ್‌-ಆನ್‌ಗಳನ್ನು ಅಶಕ್ತಗೊಳಿಸಿದ ನಂತರ
    .accesskey = R

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = deceptive ತಾಣ ವರದಿ ಮಾಡಿ…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = ಇದು ದಾಳಿಕಾರಕ ತಾಣವಲ್ಲ…‍
    .accesskey = d

## More Tools

