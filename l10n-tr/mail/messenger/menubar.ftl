# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Metin kodlaması
    .accesskey = M

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Sorun giderme modu…
    .accesskey = o
menu-help-exit-troubleshoot-mode =
    .label = Sorun giderme modunu kapat
    .accesskey = o
menu-help-more-troubleshooting-info =
    .label = Sorun giderme bilgileri
    .accesskey = b

## Mail Toolbar

toolbar-junk-button =
    .label = Gereksiz
    .tooltiptext = Seçili iletileri gereksiz olarak işaretle
toolbar-not-junk-button =
    .label = Gereksiz değil
    .tooltiptext = Seçili iletileri gereksiz değil olarak işaretle
toolbar-delete-button =
    .label = Sil
    .tooltiptext = Seçili iletileri veya klasörü sil
toolbar-undelete-button =
    .label = Silmeyi geri al
    .tooltiptext = Seçili iletileri silmeyi geri al
