# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View Menu

menu-view-charset =
    .label = Codification del texto
    .accesskey = C

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Modo diagnostic…
    .accesskey = M
menu-help-exit-troubleshoot-mode =
    .label = Disactivar le modo diagnostic
    .accesskey = D
menu-help-more-troubleshooting-info =
    .label = Altere informationes diagnostic
    .accesskey = A

## Mail Toolbar

toolbar-junk-button =
    .label = Indesirate
    .tooltiptext = Marcar le messages sequente como indesiderate
toolbar-not-junk-button =
    .label = Non indesirate
    .tooltiptext = Marcar le messages sequente como non indesiderate
toolbar-delete-button =
    .label = Deler
    .tooltiptext = Deler le messages o le plicas seligite
toolbar-undelete-button =
    .label = Restabilir
    .tooltiptext = Restabilir le messages seligite
