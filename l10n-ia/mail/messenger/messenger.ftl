# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-rights-notification-text = { -brand-short-name } es un software gratuite e open-source, producite per un communitate de milles de tote le mundo.

## Folder Pane

folder-pane-toolbar =
    .toolbarname = Barra del pannello de plicas
    .accesskey = p
folder-pane-toolbar-options-button =
    .tooltiptext = Columnas del pannello de plicas
folder-pane-header-label = Plicas

## Folder Toolbar Header Popup

folder-toolbar-hide-toolbar-toolbarbutton =
    .label = Celar le barra de instrumentos
    .accesskey = C
show-all-folders-label =
    .label = Tote le plicas
    .accesskey = T
show-unread-folders-label =
    .label = Plicas non legite
    .accesskey = n
show-favorite-folders-label =
    .label = Plicas favorite
    .accesskey = f
show-smart-folders-label =
    .label = Plicas unificate
    .accesskey = u
show-recent-folders-label =
    .label = Plicas recente
    .accesskey = r
folder-toolbar-toggle-folder-compact-view =
    .label = Visualisation compacte
    .accesskey = c
