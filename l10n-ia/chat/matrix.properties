# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (options.*):
#   These are the protocol specific options shown in the account manager and
#   account wizard windows.
options.connectServer=Servitor
options.connectPort=Porta

options.saveToken=Immagazinar le token de accesso

# LOCALIZATION NOTE (authPrompt):
#   This is the prompt in the browser window that pops up to authorize us
#   to use a Matrix account. It is shown in the title bar of the authorization
#   window.
authPrompt=Permitte le uso de tu conto Matrix

# LOCALIZATION NOTE (connection.*):
#   These will be displayed in the account manager in order to show the progress
#   of the connection.
#   (These will be displayed in account.connection.progress from
#    accounts.properties, which adds … at the end, so do not include
#    periods at the end of these messages.)
connection.requestAuth=Attendente tu autorisation
connection.requestAccess=Fin del authentication

# LOCALIZATION NOTE (connection.error.*):
#   These will show in the account manager if an error occurs during the
#   connection attempt.
connection.error.noSupportedFlow=Le servitor non forni un fluxo de session compatibile.
connection.error.authCancelled=Tu cancellava le processo de autorisation.
connection.error.sessionEnded=Le session ha essite claudite.

# LOCALIZATION NOTE (chatRoomField.*):
#   These are the name of fields displayed in the 'Join Chat' dialog
#   for Matrix accounts.
#   The _ character won't be displayed; it indicates the next
#   character of the string should be used as the access key for this
#   field.
chatRoomField.room=_Sala

# LOCALIZATION NOTE (tooltip.*):
#    These are the descriptions given in a tooltip with information received
#    from the "User" object.
# The human readable name of the user.
tooltip.displayName=Nomine a monstrar
# %S is the timespan elapsed since the last activity.
tooltip.timespan=%S retro
tooltip.lastActive=Ultime activitate

# LOCALIZATION NOTE (powerLevel.*):
#    These are the string representations of different standard power levels and strings.
#    %S are one of the power levels, Default/Moderator/Admin/Restricted/Custom.
powerLevel.default=Predefinite
powerLevel.moderator=Moderator
powerLevel.admin=Administrator
powerLevel.restricted=Limitate
powerLevel.custom=Personalisate
#    %1$S is the power level name
#    %2$S is the power level number
powerLevel.detailed=%1$S (%2$S)
powerLevel.defaultRole=Rolo ordinari: %S
powerLevel.inviteUser=Invitar usatores: %S
powerLevel.kickUsers=Ejectar usatores: %S
powerLevel.ban=Bannir usatores: %S
powerLevel.roomAvatar=Cambiar le avatar del sala: %S
powerLevel.mainAddress=Cambiar le adresse principal del sala: %S
powerLevel.history=Cambiar le visibilitate del historia: %S
powerLevel.roomName=Cambiar le nomine del sala: %S
powerLevel.changePermissions=Cambiar le permissiones: %S
powerLevel.server_acl=Inviar eventos m.room.server_acl: %S
powerLevel.upgradeRoom=Actualisar le sala: %S
powerLevel.remove=Remover messages: %S
powerLevel.events_default=Eventos ordinari: %S
powerLevel.state_default=Cambiar parametros: %S
powerLevel.encryption=Activar cryptation del sala: %S
powerLevel.topic=Definir le topico del sala: %S

# LOCALIZATION NOTE (detail.*):
#    These are the string representations of different matrix properties.
#    %S will typically be strings with the actual values.
# Example placeholder: "Foo bar"
detail.name=Nomine: %S
# Example placeholder: "My first room"
detail.topic=Argumento: %S
# Example placeholder: "5"
detail.version=Version del sala: %S
# Example placeholder: "#thunderbird:mozilla.org"
detail.roomId=RoomID: %S
# %S are all admin users. Example: "@foo:example.com, @bar:example.com"
detail.admin=Administrator: %S
# %S are all moderators. Example: "@lorem:mozilla.org, @ipsum:mozilla.org"
detail.moderator=Moderator: %S
# Example placeholder: "#thunderbird:matrix.org"
detail.alias=Alias: %S
# Example placeholder: "can_join"
detail.guest=Accesso de invitatos: %S
# This is a heading, followed by the powerLevel.* strings
detail.power=Nivellos de poter:

# LOCALIZATION NOTE (command.*):
#   These are the help messages for each command, the %S is the command name
#   Each command first gives the parameter it accepts and then a description of
#   the command.
command.ban=%S &lt;IDdelUsator&gt; [&lt;motivo&gt;]: Bannir le usator specificate del sala con un message facultative de motivo. Require le permission de bannir usatores.
command.invite=%S &lt;IDdelUsator&gt;: Invitar le usator a entrar in le sala.
command.kick=%S &lt;IDdelUsator&gt; [&lt;motivo&gt;]: Ejectar le usator specificate del sala con un message facultative de motivo. Require le permission de ejectar usatores.
command.nick=%S &lt;nove_nomine&gt;: Cambiar tu pseudonymo.

# LOCALIZATION NOTE (message.*):
#    These are shown as system messages in the conversation.
#    %S is the reason string for the particular action.
#    Used within context of ban, kick and withdrew invite.
#    Gets message.reason appended, if a reason was specified.
#    %1$S is the name of the user who banned.
#    %2$S is the name of the user who got banned.
#    %1$S is the name of the user who accepted the invitation.
#    %2$S is the name of the user who sent the invitation.
#    %S is the name of the user who accepted an invitation.
#    %1$S is the name of the user who invited.
#    %2$S is the name of the user who got invited.
message.invited=%1$S ha invitate %2$S.
#    %1$S is the name of the user who changed their display name.
#    %2$S is the old display name.
#    %3$S is the new display name.
#    %1$S is the name of the user who set their display name.
#    %2$S is the newly set display name.
#    %1$S is the name of the user who removed their display name.
#    %2$S is the old display name which has been removed.
#    %S is the name of the user who has joined the room.
#    %S is the name of the user who has rejected the invitation.
#    %S is the name of the user who has left the room.
#    %1$S is the name of the user who unbanned.
#    %2$S is the name of the user who got unbanned.
#    Gets message.reason appended, if a reason was specified.
#    %1$S is the name of the user who kicked.
#    %2$S is the name of the user who got kicked.
#    Gets message.reason appended, if a reason was specified.
#    %1$S is the name of the user who withdrew invitation.
#    %2$S is the name of the user whose invitation has been withdrawn.
#    %S is the name of the user who has removed the room name.
#    %1$S is the name of the user who changed the room name.
#    %2$S is the new room name.
#    %1$S is the name of the user who changed the power level.
#    %2$S is a list of "message.powerLevel.fromTo" strings representing power level changes separated by commas
#    power level changes, separated by commas if  there are multiple changes.
#    %1$S is the name of the target user whose power level has been changed.
#    %2$S is the old power level.
#    %2$S is the new power level.
#    %S is the name of the user who has allowed guests to join the room.
#    %S is the name of the user who has prevented guests to join the room.
#    %S is the name of the user who has made future room history visible to anyone.
#    %S is the name of the user who has made future room history visible to all room members.
#    %S is the name of the user who has made future room history visible to all room members, from the point they are invited.
#    %S is the name of the user who has made future room history visible to all room members, from the point they joined.
#    %1$S is the name of the user who changed the address.
#    %2$S is the old address.
#    %3$S is the new address.
#    %1$S is the name of the user who added the address.
#    %2$S is a comma delimited list of added addresses.
#    %1$S is the name of the user who removed the address.
#    %2$S is a comma delimited list of removed addresses.
#    %1$S is the name of the user that edited the alias addresses.
#    %2$S is a comma delimited list of removed addresses.
#    %3$S is a comma delmited list of added addresses.
