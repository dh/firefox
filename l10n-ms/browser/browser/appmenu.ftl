# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Penyesuaian…

## Zoom Controls

appmenuitem-new-window =
    .label = Tetingkap Baru
appmenuitem-new-private-window =
    .label = Tetingkap Peribadi Baru

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = Skrin Penuh

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sync Sekarang
appmenuitem-save-page =
    .label = Simpan Halaman Sebagai…

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Perihal { -brand-shorter-name }
    .accesskey = P
appmenu-help-troubleshooting-info =
    .label = Maklumat Pencarisilapan
    .accesskey = P
appmenu-help-taskmanager =
    .label = Pengurus Tugasan
appmenu-help-report-site-issue =
    .label = Laporkan Isu Laman…
appmenu-help-feedback-page =
    .label = Hantar Maklum balas…
    .accesskey = H

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Mula semula dengan Add-ons Dinyahdayakan…
    .accesskey = M
appmenu-help-safe-mode-with-addons =
    .label = Mula semula dengan Add-ons Didayakan
    .accesskey = M

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Laporkan laman yang mengelirukan…
    .accesskey = m
appmenu-help-not-deceptive =
    .label = Ini bukan laman mengelirukan…
    .accesskey = m

## More Tools

appmenu-taskmanager =
    .label = Pengurus Tugasan
