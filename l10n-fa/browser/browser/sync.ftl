# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-toolbar-sync-syncing =
    .label = همگام‌سازی…
fxa-toolbar-sync-syncing-tabs =
    .label = همگام‌سازی زبانه‌ها…
fxa-toolbar-sync-syncing2 = همگام‌سازی…
sync-disconnect-dialog-title = قطع ارتباط با { -sync-brand-short-name }؟
sync-disconnect-dialog-body = { -brand-product-name } همگام‌سازی با حساب شما را متوقف می‌کند اما هیچ یک از داده‌های مرور شما بر روی این دستگاه را پاک نخواهد کرد.
fxa-disconnect-dialog-title = قطع ارتباط با { -brand-product-name }؟
fxa-disconnect-dialog-body = { -brand-product-name } اتصال با حساب شما را قطع می‌کند اما هیچ یک از داده‌های مرور شما بر روی این دستگاه را پاک نخواهد کرد.
sync-disconnect-dialog-button = قطع ارتباط
fxa-signout-dialog-heading = از { -fxaccount-brand-name } خارج می‌شوید؟
fxa-signout-dialog-body = داده‌های همگام‌سازی شده در حساب شما باقی می‌مانند.
fxa-signout-checkbox =
    .label = حذف داده‌ها از این دستگاه (ورودها، گذرواژه‌ها، تاریخچه، نشانک‌ها و غیره).
fxa-signout-dialog =
    .title = خروج از { -fxaccount-brand-name }
    .style = min-width: 375px;
    .buttonlabelaccept = خروج
fxa-menu-connect-another-device =
    .label = اتصال به دستگاه دیگر…
fxa-menu-sign-out =
    .label = خروج…
