# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The question portion of the following message should have the <strong> and </strong> tags surrounding it.
default-browser-notification-message = <strong>تنظیم { -brand-short-name } به عنوان مرورگر پیش‌فرض؟</strong> یک مرورگرِ سریع، امن و ناشناس در هنگام مرور وب داشته باشید.
default-browser-notification-button =
    .label = تنظیم به عنوان پیش‌فرض
    .accesskey = S
