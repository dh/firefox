# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Sérsníða…

## Zoom Controls

appmenuitem-new-window =
    .label = Nýr gluggi
appmenuitem-new-private-window =
    .label = Nýr huliðsgluggi

## Zoom and Fullscreen Controls

appmenuitem-fullscreen =
    .label = Fylla skjá

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Samstilla núna
appmenuitem-save-page =
    .label = Vista síðu sem…

## What's New panel in App menu.


## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Um { -brand-shorter-name }
    .accesskey = U
appmenu-help-troubleshooting-info =
    .label = Upplýsingar fyrir úrræðaleit
    .accesskey = t
appmenu-help-taskmanager =
    .label = Verkefnisstjóri
appmenu-help-report-site-issue =
    .label = Tilkynna vandamál á vefsvæði…
appmenu-help-feedback-page =
    .label = Senda álit…
    .accesskey = S

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Endurræsa með viðbætur óvirkar…
    .accesskey = r
appmenu-help-safe-mode-with-addons =
    .label = Endurræsa með viðbætur virkar
    .accesskey = r

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Tilkynna svindlsvæði…
    .accesskey = d
appmenu-help-not-deceptive =
    .label = Þetta er ekki svindlsvæði…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = Verkefnisstjóri
