# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

## View Menu

menu-view-charset =
    .label = Textkodierung
    .accesskey = T

## Mail Toolbar

toolbar-junk-button =
    .label = Junk
    .tooltiptext = Gewählte Nachricht(en) als Junk einstufen
toolbar-not-junk-button =
    .label = Kein Junk
    .tooltiptext = Gewählte Nachricht(en) als "Kein Junk" einstufen
toolbar-delete-button =
    .label = Löschen
    .tooltiptext = Gewählte(n) Nachricht(en) oder Ordner löschen
toolbar-undelete-button =
    .label = Wiederherstellen
    .tooltiptext = Löschen gewählter Nachricht(en) rückgängig machen
