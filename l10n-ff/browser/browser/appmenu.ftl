# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## App Menu

appmenuitem-customize-mode =
    .label = Heertin…

## Zoom Controls

appmenuitem-new-window =
    .label = Henorde Hesere
appmenuitem-new-private-window =
    .label = Henorde Suturo Hesere

## Zoom and Fullscreen Controls

appmenuitem-zoom-enlarge =
    .label = Lonngo ara
appmenuitem-zoom-reduce =
    .label = Lonngo woɗɗa
appmenuitem-fullscreen =
    .label = Njaajeendi Yaynirde

## Firefox Account toolbar button and Sync panel in App menu.

fxa-toolbar-sync-now =
    .label = Sanngoɗin Jooni
appmenuitem-save-page =
    .label = Danndu Hello e Innde…

## What's New panel in App menu.

whatsnew-panel-header = Ko Hesɗi

## The Firefox Profiler – The popup is the UI to turn on the profiler, and record
## performance profiles. To enable it go to profiler.firefox.com and click
## "Enable Profiler Menu Button".


## History panel


## Help panel

appmenu-about =
    .label = Baɗte { -brand-shorter-name }
    .accesskey = B
appmenu-help-troubleshooting-info =
    .label = Humpito Ñawndugol Caɗeele
    .accesskey = H
appmenu-help-taskmanager =
    .label = Yiilorde golle
appmenu-help-report-site-issue =
    .label = Jaŋto Caɗeele Lowre…
appmenu-help-feedback-page =
    .label = Neldu Duttinal…
    .accesskey = N

## appmenu-help-safe-mode-without-addons and appmenu-help-safe-mode-without-addons
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-safe-mode-without-addons =
    .label = Hurmitin tawa Ɓeyditte ena Ndaaƴaa…
    .accesskey = H
appmenu-help-safe-mode-with-addons =
    .label = Hurmitin tawa Ɓeyditte ena Ndaaƴtaa…
    .accesskey = H

## appmenu-help-enter-troubleshoot-mode and appmenu-help-exit-troubleshoot-mode
## are mutually exclusive, so it's possible to use the same accesskey for both.


## appmenu-help-report-deceptive-site and appmenu-help-not-deceptive
## are mutually exclusive, so it's possible to use the same accesskey for both.

appmenu-help-report-deceptive-site =
    .label = Jaŋto lowre fuuntoore
    .accesskey = w
appmenu-help-not-deceptive =
    .label = Ɗum wonaa lowre fuuntoore…
    .accesskey = d

## More Tools

appmenu-taskmanager =
    .label = Yiilorde golle
