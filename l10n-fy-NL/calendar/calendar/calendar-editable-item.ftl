# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendar-editable-item-privacy-icon-private =
    .alt = Privacy: Privee barren
calendar-editable-item-privacy-icon-confidential =
    .alt = Privacy: Allinnich tiid en datum toane
